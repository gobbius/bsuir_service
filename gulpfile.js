var gulp = require("gulp");
var cssmin = require("gulp-cssmin");
var rename = require("gulp-rename");
var concatCss = require('gulp-concat-css');

gulp.task('default', function () {
    return gulp.src('assets/**/*.css')
        .pipe(concatCss("custom-bundle.css"))
        .pipe(rename({suffix: ".min"}))
        .pipe(gulp.dest('dist/css'));
});

gulp.task("watch", function () {
    gulp.watch("styles/**/*.css", ["default"]);
});