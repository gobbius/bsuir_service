import {Http, Headers} from "angular2/http";
import {Injectable} from "angular2/core";
import "rxjs/add/operator/map";

import {CustomConfig} from "../../shared/config/config";
import {PermissionService} from "../../shared/service/permission.service";
import {Order} from "../../hotel/domain/Order";
import {Observable} from "rxjs/Observable";
import {CookieService} from "angular2-cookie/core";

@Injectable()
export class UserService {

    private _serviceUrl:string = CustomConfig.SERVICE_ENDPOINT;
    private _headers:Headers = new Headers({
        "Authorization": this._permissionService.authorization,
        "Accept": "application/json"
    });
    private _userSheduler:any;

    constructor(private _http:Http, private _permissionService:PermissionService, private _cookieService:CookieService) {

    }

    login(username, password) {
        var loginUrl = this._serviceUrl + "/login";
        var creds = "username=" + username + "&password=" + password + "&grant_type=password";
        var headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded"
        });
        return this._http.post(loginUrl, creds, {
            headers: headers
        }).map(result => result.json());

    }

    startSheduller(authorization:string) {
        var ordersCountUrl = this._serviceUrl + CustomConfig.API_PREFIX_MAP["orderscount"];
        this._userSheduler = setInterval(() => {
            this._http.get(ordersCountUrl, {
                headers: new Headers({
                    "Authorization": authorization,
                    "Accept": "application/json"
                })
            })
                .subscribe(result => {
                        this._permissionService.userOrdersCount = result.json();

                    },
                    error => {
                        console.log(error);
                    });
        }, 10000);
    }

    stopSheduler() {
        clearInterval(this._userSheduler);
    }

    signup(username, password, confirm) {
        var signupUrl = this._serviceUrl + CustomConfig.API_PREFIX_MAP["signup"];
        var userInfo = "username=" + username + "&password=" + password + "&confirmpassword=" + confirm;
        var headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded"
        });

        return this._http.post(signupUrl, userInfo, {
            headers: headers
        });
    }

    getUserOrders():Observable<Order[]> {
        var ordersUrl = this._serviceUrl + CustomConfig.API_PREFIX_MAP["orders"];
        return this._http.get(ordersUrl, {
            headers: this._headers
        }).map(result => {
            var orders:Order[] = [];
            var resultJson = result.json();

            resultJson.forEach(obj => {
                var tempOrder = new Order({});
                tempOrder.arrivalDate = new Date(obj["arrivalDate"]);
                tempOrder.countOfDays = obj["countOfDays"];
                tempOrder.hotelName = obj["hotelName"];
                tempOrder.id = obj["id"];
                tempOrder.leaveDate = new Date(obj["leaveDate"]);
                tempOrder.roomNumber = obj["roomNumber"];
                tempOrder.totalPrice = obj["totalPrice"];
                tempOrder.userName = obj["userName"];
                orders.push(tempOrder);
            });
            return orders;
        });
    }
}