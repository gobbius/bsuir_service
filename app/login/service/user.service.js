System.register(["angular2/http", "angular2/core", "rxjs/add/operator/map", "../../shared/config/config", "../../shared/service/permission.service", "../../hotel/domain/Order", "angular2-cookie/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var http_1, core_1, config_1, permission_service_1, Order_1, core_2;
    var UserService;
    return {
        setters:[
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (_1) {},
            function (config_1_1) {
                config_1 = config_1_1;
            },
            function (permission_service_1_1) {
                permission_service_1 = permission_service_1_1;
            },
            function (Order_1_1) {
                Order_1 = Order_1_1;
            },
            function (core_2_1) {
                core_2 = core_2_1;
            }],
        execute: function() {
            UserService = (function () {
                function UserService(_http, _permissionService, _cookieService) {
                    this._http = _http;
                    this._permissionService = _permissionService;
                    this._cookieService = _cookieService;
                    this._serviceUrl = config_1.CustomConfig.SERVICE_ENDPOINT;
                    this._headers = new http_1.Headers({
                        "Authorization": this._permissionService.authorization,
                        "Accept": "application/json"
                    });
                }
                UserService.prototype.login = function (username, password) {
                    var loginUrl = this._serviceUrl + "/login";
                    var creds = "username=" + username + "&password=" + password + "&grant_type=password";
                    var headers = new http_1.Headers({
                        "Content-Type": "application/x-www-form-urlencoded"
                    });
                    return this._http.post(loginUrl, creds, {
                        headers: headers
                    }).map(function (result) { return result.json(); });
                };
                UserService.prototype.startSheduller = function (authorization) {
                    var _this = this;
                    var ordersCountUrl = this._serviceUrl + config_1.CustomConfig.API_PREFIX_MAP["orderscount"];
                    this._userSheduler = setInterval(function () {
                        _this._http.get(ordersCountUrl, {
                            headers: new http_1.Headers({
                                "Authorization": authorization,
                                "Accept": "application/json"
                            })
                        })
                            .subscribe(function (result) {
                            _this._permissionService.userOrdersCount = result.json();
                        }, function (error) {
                            console.log(error);
                        });
                    }, 10000);
                };
                UserService.prototype.stopSheduler = function () {
                    clearInterval(this._userSheduler);
                };
                UserService.prototype.signup = function (username, password, confirm) {
                    var signupUrl = this._serviceUrl + config_1.CustomConfig.API_PREFIX_MAP["signup"];
                    var userInfo = "username=" + username + "&password=" + password + "&confirmpassword=" + confirm;
                    var headers = new http_1.Headers({
                        "Content-Type": "application/x-www-form-urlencoded"
                    });
                    return this._http.post(signupUrl, userInfo, {
                        headers: headers
                    });
                };
                UserService.prototype.getUserOrders = function () {
                    var ordersUrl = this._serviceUrl + config_1.CustomConfig.API_PREFIX_MAP["orders"];
                    return this._http.get(ordersUrl, {
                        headers: this._headers
                    }).map(function (result) {
                        var orders = [];
                        var resultJson = result.json();
                        resultJson.forEach(function (obj) {
                            var tempOrder = new Order_1.Order({});
                            tempOrder.arrivalDate = new Date(obj["arrivalDate"]);
                            tempOrder.countOfDays = obj["countOfDays"];
                            tempOrder.hotelName = obj["hotelName"];
                            tempOrder.id = obj["id"];
                            tempOrder.leaveDate = new Date(obj["leaveDate"]);
                            tempOrder.roomNumber = obj["roomNumber"];
                            tempOrder.totalPrice = obj["totalPrice"];
                            tempOrder.userName = obj["userName"];
                            orders.push(tempOrder);
                        });
                        return orders;
                    });
                };
                UserService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http, permission_service_1.PermissionService, core_2.CookieService])
                ], UserService);
                return UserService;
            }());
            exports_1("UserService", UserService);
        }
    }
});
//# sourceMappingURL=user.service.js.map