import {Component, OnInit} from "angular2/core";
import "rxjs/add/operator/map";

import {UserService} from "../service/user.service";
import {CookieService} from "angular2-cookie/core";
import {Router} from "angular2/router";
import {PermissionService} from "../../shared/service/permission.service";
import {ControlGroup, FormBuilder, Validators} from "angular2/common";
import {PasswordValidator} from "../../shared/validator/password.validator";


@Component({
    selector: "signup-form",
    templateUrl: "app/login/view/signup.template.html",
    styles: [`
        @media (min-width: 992px) {
            .custom-container {
                margin-top: 5% !important;
            }
        }
            
        .custom-container {
            margin-top: 15%;
        }    
        .ng-touched.ng-invalid{
            border-color: red;
        }
        .login-form {
            background-color: #f9f2f4;
            border-radius: 2%;
            color: black;
        }
        .login-form legend {
            color: black;
        }
        .login-form button {
            width: 50%;
        }
        .spinner {
            display: inline-block;
            opacity: 0;
            width: 0;
        
            -webkit-transition: opacity 0.25s, width 0.25s;
            -moz-transition: opacity 0.25s, width 0.25s;
            -o-transition: opacity 0.25s, width 0.25s;
            transition: opacity 0.25s, width 0.25s;
        }
        .has-spinner.active {
            cursor:progress;
        }
        
        .has-spinner.active .spinner {
            opacity: 1;
            width: auto; /* This doesn't work, just fix for unkown width elements */
        }
        
        .has-spinner.btn-mini.active .spinner {
            width: 10px;
        }
        
        .has-spinner.btn-small.active .spinner {
            width: 13px;
        }
        
        .has-spinner.btn.active .spinner {
            width: 16px;
        }
        
        .has-spinner.btn-large.active .spinner {
            width: 19px;
}      
    `]
})

export class SignUpComponent implements OnInit {

    private _isUserExist:boolean = false;
    private _form:ControlGroup;
    private _isConfirmed:boolean = false;

    constructor(private _userService:UserService,
                private _router:Router,
                formBuilder:FormBuilder) {
        this._form = formBuilder.group({
            username: [null, Validators.required],
            password: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
            confirm: [null, Validators.required]
        }, {validator: PasswordValidator.confirmPassword});
    }

    get isUserExist():boolean {
        return this._isUserExist;
    }

    get form():ControlGroup {
        return this._form;
    }

    get isConfirmed():boolean {
        return this._isConfirmed;
    }

    ngOnInit() {
        $("#signUpBtn").click(function () {
            $(this).toggleClass("active");
        });
    }

    signUp() {
        $(this).prop("disabled", true);
        this._userService.signup(this._form.find("username").value,
            this._form.find("password").value,
            this._form.find("confirm").value)
            .subscribe(
                result => {
                    this._isConfirmed = true;
                },
                error=> {
                    console.error(error);
                    var errorJson = JSON.parse(error._body);
                    if (_.has(errorJson, "message")) {
                        this._isUserExist = true;
                        _.delay(()=>this._isUserExist = false, 2000);
                    }
                    $("#signUpBtn").toggleClass("active");
                    $("#signUpBtn").prop("disabled", false);
                },
                () => {
                    $("#signUpBtn").toggleClass("active");
                    $("#signUpBtn").prop("disabled", false);
                    this._isUserExist = false;
                    _.delay(()=>this._router.navigate(["Login"]), 2000);

                });
    }

}