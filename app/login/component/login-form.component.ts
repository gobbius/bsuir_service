import {Component, OnInit} from "angular2/core";
import "rxjs/add/operator/map";

import {UserService} from "../service/user.service";
import {CookieService} from "angular2-cookie/core";
import {Router, ROUTER_DIRECTIVES} from "angular2/router";
import {PermissionService} from "../../shared/service/permission.service";


@Component({
    selector: "login-form",
    templateUrl: "app/login/view/login-form.template.html",
    styles: [`
        @media (min-width: 992px) {
            .custom-container {
                margin-top: 5% !important;
            }
        }
            
        .custom-container {
            margin-top: 15%;
        }    
        .ng-touched.ng-invalid{
            border-color: red;
        }
        .login-form {
            background-color: #f9f2f4;
            border-radius: 2%;
            color: black;
        }
        .login-form legend {
            color: black;
        }
        .login-form button {
            width: 40%;
        }
        .spinner {
            display: inline-block;
            opacity: 0;
            width: 0;
        
            -webkit-transition: opacity 0.25s, width 0.25s;
            -moz-transition: opacity 0.25s, width 0.25s;
            -o-transition: opacity 0.25s, width 0.25s;
            transition: opacity 0.25s, width 0.25s;
        }
        .has-spinner.active {
            cursor:progress;
        }
        
        .has-spinner.active .spinner {
            opacity: 1;
            width: auto; /* This doesn't work, just fix for unkown width elements */
        }
        
        .has-spinner.btn-mini.active .spinner {
            width: 10px;
        }
        
        .has-spinner.btn-small.active .spinner {
            width: 13px;
        }
        
        .has-spinner.btn.active .spinner {
            width: 16px;
        }
        
        .has-spinner.btn-large.active .spinner {
            width: 19px;
}      
    `],
    directives: [ROUTER_DIRECTIVES]
})

export class LoginComponent implements OnInit {

    private _isServiceError:boolean = false;
    private _isErrorInCreds:boolean = false;

    constructor(private _userService:UserService,
                private _cookieService:CookieService,
                private _permissionService:PermissionService,
                private _router:Router) {
    }

    get isErrorInCreds():boolean {
        return this._isErrorInCreds;
    }

    ngOnInit() {
        $("#loginBtn").click(function () {
            $(this).toggleClass("active");

        });
    }

    submitForm(form) {
        $("#loginBtn").prop("disabled", true);
        this._userService.login(form.login, form.password)
            .subscribe(
                result => {
                    this._cookieService.put("access_token", result["access_token"]);
                    this._cookieService.put("token_type", result["token_type"]);
                    this._cookieService.put("expires_in", result["expires_in"]);
                    this._cookieService.put("current_user", form.login);
                    this._permissionService.isInternal = true;
                    this._permissionService.authorization = result["token_type"] + " " + result["access_token"];
                    this._permissionService.currentUser = <string>form.login.toLowerCase();
                    this._router.navigate(["../../Home"]);
                },
                error=> {
                    $("#loginBtn").toggleClass("active");
                    $("#loginBtn").prop("disabled", false);
                    console.error(error);
                    var errorObj = JSON.parse(error._body);
                    this._isErrorInCreds = (errorObj != null
                    && errorObj.error_description != null) ? true : false;
                    this._isServiceError = (errorObj == null) ? true : false;
                },
                () => {
                    this._userService.startSheduller(this._permissionService.authorization);
                    this._isServiceError = false;
                    this._isErrorInCreds = false;
                    $("#loginBtn").toggleClass("active");
                    $("#loginBtn").prop("disabled", false);
                });
    }

}