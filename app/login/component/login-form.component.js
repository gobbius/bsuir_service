System.register(["angular2/core", "rxjs/add/operator/map", "../service/user.service", "angular2-cookie/core", "angular2/router", "../../shared/service/permission.service"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, user_service_1, core_2, router_1, permission_service_1;
    var LoginComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (_1) {},
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            },
            function (core_2_1) {
                core_2 = core_2_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (permission_service_1_1) {
                permission_service_1 = permission_service_1_1;
            }],
        execute: function() {
            LoginComponent = (function () {
                function LoginComponent(_userService, _cookieService, _permissionService, _router) {
                    this._userService = _userService;
                    this._cookieService = _cookieService;
                    this._permissionService = _permissionService;
                    this._router = _router;
                    this._isServiceError = false;
                    this._isErrorInCreds = false;
                }
                Object.defineProperty(LoginComponent.prototype, "isErrorInCreds", {
                    get: function () {
                        return this._isErrorInCreds;
                    },
                    enumerable: true,
                    configurable: true
                });
                LoginComponent.prototype.ngOnInit = function () {
                    $("#loginBtn").click(function () {
                        $(this).toggleClass("active");
                    });
                };
                LoginComponent.prototype.submitForm = function (form) {
                    var _this = this;
                    $("#loginBtn").prop("disabled", true);
                    this._userService.login(form.login, form.password)
                        .subscribe(function (result) {
                        _this._cookieService.put("access_token", result["access_token"]);
                        _this._cookieService.put("token_type", result["token_type"]);
                        _this._cookieService.put("expires_in", result["expires_in"]);
                        _this._cookieService.put("current_user", form.login);
                        _this._permissionService.isInternal = true;
                        _this._permissionService.authorization = result["token_type"] + " " + result["access_token"];
                        _this._permissionService.currentUser = form.login.toLowerCase();
                        _this._router.navigate(["../../Home"]);
                    }, function (error) {
                        $("#loginBtn").toggleClass("active");
                        $("#loginBtn").prop("disabled", false);
                        console.error(error);
                        var errorObj = JSON.parse(error._body);
                        _this._isErrorInCreds = (errorObj != null
                            && errorObj.error_description != null) ? true : false;
                        _this._isServiceError = (errorObj == null) ? true : false;
                    }, function () {
                        _this._userService.startSheduller(_this._permissionService.authorization);
                        _this._isServiceError = false;
                        _this._isErrorInCreds = false;
                        $("#loginBtn").toggleClass("active");
                        $("#loginBtn").prop("disabled", false);
                    });
                };
                LoginComponent = __decorate([
                    core_1.Component({
                        selector: "login-form",
                        templateUrl: "app/login/view/login-form.template.html",
                        styles: ["\n        @media (min-width: 992px) {\n            .custom-container {\n                margin-top: 5% !important;\n            }\n        }\n            \n        .custom-container {\n            margin-top: 15%;\n        }    \n        .ng-touched.ng-invalid{\n            border-color: red;\n        }\n        .login-form {\n            background-color: #f9f2f4;\n            border-radius: 2%;\n            color: black;\n        }\n        .login-form legend {\n            color: black;\n        }\n        .login-form button {\n            width: 40%;\n        }\n        .spinner {\n            display: inline-block;\n            opacity: 0;\n            width: 0;\n        \n            -webkit-transition: opacity 0.25s, width 0.25s;\n            -moz-transition: opacity 0.25s, width 0.25s;\n            -o-transition: opacity 0.25s, width 0.25s;\n            transition: opacity 0.25s, width 0.25s;\n        }\n        .has-spinner.active {\n            cursor:progress;\n        }\n        \n        .has-spinner.active .spinner {\n            opacity: 1;\n            width: auto; /* This doesn't work, just fix for unkown width elements */\n        }\n        \n        .has-spinner.btn-mini.active .spinner {\n            width: 10px;\n        }\n        \n        .has-spinner.btn-small.active .spinner {\n            width: 13px;\n        }\n        \n        .has-spinner.btn.active .spinner {\n            width: 16px;\n        }\n        \n        .has-spinner.btn-large.active .spinner {\n            width: 19px;\n}      \n    "],
                        directives: [router_1.ROUTER_DIRECTIVES]
                    }), 
                    __metadata('design:paramtypes', [user_service_1.UserService, core_2.CookieService, permission_service_1.PermissionService, router_1.Router])
                ], LoginComponent);
                return LoginComponent;
            }());
            exports_1("LoginComponent", LoginComponent);
        }
    }
});
//# sourceMappingURL=login-form.component.js.map