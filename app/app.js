System.register(["angular2/core", "angular2/router", "./shared/component/notfound.component", "./shared/component/home.component", "./shared/router/navigation.router", "./shared/component/permission-denied.component", "./shared/component/navbar.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, notfound_component_1, home_component_1, navigation_router_1, permission_denied_component_1, navbar_component_1;
    var App;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (notfound_component_1_1) {
                notfound_component_1 = notfound_component_1_1;
            },
            function (home_component_1_1) {
                home_component_1 = home_component_1_1;
            },
            function (navigation_router_1_1) {
                navigation_router_1 = navigation_router_1_1;
            },
            function (permission_denied_component_1_1) {
                permission_denied_component_1 = permission_denied_component_1_1;
            },
            function (navbar_component_1_1) {
                navbar_component_1 = navbar_component_1_1;
            }],
        execute: function() {
            App = (function () {
                function App() {
                }
                App = __decorate([
                    router_1.RouteConfig([
                        {
                            path: "/not-found",
                            name: "NotFound",
                            component: notfound_component_1.NotFoundComponent
                        },
                        {
                            path: "/navigation/...",
                            name: "Navigation",
                            component: navigation_router_1.NavigationComponent
                        },
                        {
                            path: "/",
                            name: "Home",
                            component: home_component_1.HomeComponent,
                            useAsDefault: true
                        },
                        {
                            path: "/*other",
                            name: "Other",
                            redirectTo: ["Home"]
                        },
                        {
                            path: "/denied",
                            name: "Denied",
                            component: permission_denied_component_1.PermissionsDeniedComponent,
                        }
                    ]),
                    core_1.Component({
                        selector: "main",
                        templateUrl: "app/app.template.html",
                        directives: [navbar_component_1.NavbarComponent]
                    }), 
                    __metadata('design:paramtypes', [])
                ], App);
                return App;
            }());
            exports_1("App", App);
        }
    }
});
//# sourceMappingURL=app.js.map