System.register(["angular2/core", "angular2/router", "../../login/component/login-form.component", "../../hotel/router/hotels.router", "../../login/component/signup.component", "../../hotel/component/orders.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, login_form_component_1, hotels_router_1, signup_component_1, orders_component_1;
    var NavigationComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (login_form_component_1_1) {
                login_form_component_1 = login_form_component_1_1;
            },
            function (hotels_router_1_1) {
                hotels_router_1 = hotels_router_1_1;
            },
            function (signup_component_1_1) {
                signup_component_1 = signup_component_1_1;
            },
            function (orders_component_1_1) {
                orders_component_1 = orders_component_1_1;
            }],
        execute: function() {
            NavigationComponent = (function () {
                function NavigationComponent() {
                }
                NavigationComponent = __decorate([
                    router_1.RouteConfig([
                        {
                            path: "/login",
                            name: "Login",
                            component: login_form_component_1.LoginComponent
                        },
                        {
                            path: "/signup",
                            name: "SignUp",
                            component: signup_component_1.SignUpComponent
                        },
                        {
                            path: "/orders",
                            name: "Orders",
                            component: orders_component_1.OrdersComponent
                        },
                        {
                            path: "/hotels/...",
                            name: "HotelRoute",
                            component: hotels_router_1.HotelRouter
                        }
                    ]),
                    core_1.Component({
                        selector: "navigation",
                        template: "<div class='container-fluid'>\n                    <router-outlet></router-outlet>\n                </div>",
                        directives: [router_1.ROUTER_DIRECTIVES]
                    }), 
                    __metadata('design:paramtypes', [])
                ], NavigationComponent);
                return NavigationComponent;
            }());
            exports_1("NavigationComponent", NavigationComponent);
        }
    }
});
//# sourceMappingURL=navigation.router.js.map