import {Component, OnInit, OnDestroy} from "angular2/core";
import {ROUTER_DIRECTIVES, Router, RouteConfig, OnActivate, OnDeactivate} from "angular2/router";
import {LoginComponent} from "../../login/component/login-form.component";
import {HotelRouter} from "../../hotel/router/hotels.router";
import {SignUpComponent} from "../../login/component/signup.component";
import {OrdersComponent} from "../../hotel/component/orders.component";


@RouteConfig([
    {
        path: "/login",
        name: "Login",
        component: LoginComponent
    },
    {
        path: "/signup",
        name: "SignUp",
        component: SignUpComponent
    },
    {
        path: "/orders",
        name: "Orders",
        component: OrdersComponent
    },
    {
        path: "/hotels/...",
        name: "HotelRoute",
        component: HotelRouter
    }
])

@Component({
    selector: "navigation",
    template: `<div class='container-fluid'>
                    <router-outlet></router-outlet>
                </div>`,
    directives: [ROUTER_DIRECTIVES]
})

export class NavigationComponent {

    constructor() {
    }
    
    


}