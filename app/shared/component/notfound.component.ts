import {Component} from "angular2/core";

@Component({
    selector: "not-found",
    template: "<div class='container' style='margin-top: 10%;'><h1>Not found</h1></div>"
})

export class NotFoundComponent {

}