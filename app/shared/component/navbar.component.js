System.register(["angular2/core", "angular2/router", "../service/permission.service", "angular2-cookie/core", "../../login/service/user.service"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, permission_service_1, core_2, user_service_1;
    var NavbarComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (permission_service_1_1) {
                permission_service_1 = permission_service_1_1;
            },
            function (core_2_1) {
                core_2 = core_2_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            }],
        execute: function() {
            NavbarComponent = (function () {
                function NavbarComponent(_permissionService, _cookieService, _userService, _router) {
                    this._permissionService = _permissionService;
                    this._cookieService = _cookieService;
                    this._userService = _userService;
                    this._router = _router;
                }
                Object.defineProperty(NavbarComponent.prototype, "isInternal", {
                    get: function () {
                        return this._permissionService.isInternal;
                    },
                    set: function (value) {
                        this._permissionService.isInternal = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(NavbarComponent.prototype, "currentUser", {
                    get: function () {
                        return this._permissionService.currentUser;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(NavbarComponent.prototype, "ordersCount", {
                    get: function () {
                        return this._permissionService.userOrdersCount;
                    },
                    enumerable: true,
                    configurable: true
                });
                NavbarComponent.prototype.loadJquery = function () {
                    var _this = this;
                    var interval = setInterval(function () {
                        $(".tab").click(function ($event) {
                            var currentTab = $event.target;
                            $(".tab").each(function (index, elem) {
                                if ($(elem).text() != "Home") {
                                    $(elem).removeClass("active");
                                }
                            });
                            $(currentTab).addClass("active");
                        });
                        if (document.getElementsByClassName("tab").length > 0) {
                            clearInterval(interval);
                            if (!_.isEmpty(_this._permissionService.authorization)) {
                                _this._userService.startSheduller(_this._permissionService.authorization);
                            }
                        }
                    }, 100);
                };
                NavbarComponent.prototype.ngOnInit = function () {
                    this.loadJquery();
                };
                NavbarComponent.prototype.logout = function () {
                    this._cookieService.removeAll();
                    this._userService.stopSheduler();
                    this._permissionService.userOrdersCount = null;
                    this._permissionService.isInternal = false;
                    this._router.navigate(['Home']);
                };
                NavbarComponent = __decorate([
                    core_1.Component({
                        selector: "navbar",
                        templateUrl: "app/shared/view/navigation.template.html",
                        directives: [router_1.ROUTER_DIRECTIVES],
                        providers: [core_2.CookieService],
                        styles: ["\n            .tab {\n                color: black !important;\n                background-color: white !important;\n            }\n            .tab:hover {\n                color: white !important;\n                background-color: #2196f3 !important;\n            }\n            .active {\n                color: white !important;\n                background-color: #2196f3 !important;\n            }\n            .btn-group {\n                margin-top: 1%;\n            }\n            .user {\n                margin-top: -2%;\n            }\n            .nav-container{\n                padding-left: 0px !important;\n            }\n"]
                    }), 
                    __metadata('design:paramtypes', [permission_service_1.PermissionService, core_2.CookieService, user_service_1.UserService, router_1.Router])
                ], NavbarComponent);
                return NavbarComponent;
            }());
            exports_1("NavbarComponent", NavbarComponent);
        }
    }
});
//# sourceMappingURL=navbar.component.js.map