System.register(["angular2/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var PaginationComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            PaginationComponent = (function () {
                function PaginationComponent() {
                    this._currentPage = 1;
                    this._numberOfPages = [];
                    this._isPrevDisabled = true;
                    this._isNextDisabled = false;
                    this.changePage = new core_1.EventEmitter();
                }
                Object.defineProperty(PaginationComponent.prototype, "numberOfPages", {
                    get: function () {
                        return this._numberOfPages;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(PaginationComponent.prototype, "currentPage", {
                    get: function () {
                        return this._currentPage;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(PaginationComponent.prototype, "isNextDisabled", {
                    get: function () {
                        return this._isNextDisabled;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(PaginationComponent.prototype, "isPrevDisabled", {
                    get: function () {
                        return this._isPrevDisabled;
                    },
                    enumerable: true,
                    configurable: true
                });
                PaginationComponent.prototype.ngOnInit = function () {
                    for (var i = 0; i <= this.items.length / this.pageSize; i++) {
                        this._numberOfPages.push(i + 1);
                    }
                };
                PaginationComponent.prototype.onClick = function (page) {
                    this._currentPage = page;
                    this._isPrevDisabled = this._currentPage > 1 ? false : true;
                    this._isNextDisabled = this._currentPage < this.numberOfPages.length ? false : true;
                    var offset = (this._currentPage - 1) * this.pageSize;
                    var end = offset + this.pageSize;
                    this.changePage.emit({
                        pageNumber: this._currentPage,
                        offset: offset,
                        end: end
                    });
                };
                PaginationComponent.prototype.onNext = function () {
                    if (this._currentPage == this.numberOfPages.length) {
                        return;
                    }
                    this.onClick(this._currentPage += 1);
                };
                PaginationComponent.prototype.onPrev = function () {
                    if (this._currentPage == 1) {
                        this._isPrevDisabled = true;
                        return;
                    }
                    this.onClick(this._currentPage -= 1);
                };
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Array)
                ], PaginationComponent.prototype, "items", void 0);
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Number)
                ], PaginationComponent.prototype, "pageSize", void 0);
                __decorate([
                    core_1.Output("page-change"), 
                    __metadata('design:type', Object)
                ], PaginationComponent.prototype, "changePage", void 0);
                PaginationComponent = __decorate([
                    core_1.Component({
                        selector: "pagination",
                        templateUrl: "app/shared/view/pagination.template.html",
                        styles: ["\n        a {\n            cursor: pointer;\n        }\n"]
                    }), 
                    __metadata('design:paramtypes', [])
                ], PaginationComponent);
                return PaginationComponent;
            }());
            exports_1("PaginationComponent", PaginationComponent);
        }
    }
});
//# sourceMappingURL=pagination.component.js.map