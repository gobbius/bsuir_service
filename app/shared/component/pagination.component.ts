import {Component, Input, Output, EventEmitter, OnInit} from "angular2/core";

@Component({
    selector: "pagination",
    templateUrl: "app/shared/view/pagination.template.html",
    styles: [`
        a {
            cursor: pointer;
        }
`]
})

export class PaginationComponent implements OnInit {

    @Input() items:any[];
    @Input() pageSize:number;
    private _currentPage:number = 1;
    private _numberOfPages:number[] = [];
    private _isPrevDisabled:boolean = true;
    private _isNextDisabled:boolean = false;
    @Output("page-change") changePage;

    constructor() {
        this.changePage = new EventEmitter();
    }

    get numberOfPages():number[] {
        return this._numberOfPages;
    }

    get currentPage():number {
        return this._currentPage;
    }

    get isNextDisabled():boolean {
        return this._isNextDisabled;
    }

    get isPrevDisabled():boolean {
        return this._isPrevDisabled;
    }

    ngOnInit() {

        for (var i = 0; i <= this.items.length / this.pageSize; i++) {
            this._numberOfPages.push(i + 1);
        }
    }

    onClick(page) {

        this._currentPage = page;

        this._isPrevDisabled = this._currentPage > 1 ? false : true;
        this._isNextDisabled = this._currentPage < this.numberOfPages.length ? false : true;

        var offset = (this._currentPage - 1) * this.pageSize;
        var end = offset + this.pageSize;

        this.changePage.emit({
            pageNumber: this._currentPage,
            offset: offset,
            end: end
        });
    }

    onNext() {

        if (this._currentPage == this.numberOfPages.length) {
            return;
        }

        this.onClick(this._currentPage += 1);
    }

    onPrev() {
        if (this._currentPage == 1) {
            this._isPrevDisabled = true;
            return;
        }
        this.onClick(this._currentPage -= 1);
    }

}