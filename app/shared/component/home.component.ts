import {Component} from "angular2/core";

@Component({
    selector: "home",
    templateUrl: "app/shared/view/home.template.html",
    styles: [`
                .custom-container {
                    margin-top: 25%;
                }
                .custom-brands {
                    margin-top: 10%;
                }
                
                .custom-brands>h3 {
                    margin-left: 25%;
                }
                
                .custom-hosts ul {
                    margin-left: 17%;
                }
                
                h3 {
                    font-family: fantasy;
                }
                

                ul {
                    list-style: none;
                }
                ul>li {
                    display: inline;
                    padding-right: 10%;
                }
                
                @media (min-width: 992px) {
                    .custom-container {
                        margin-top: 5% !important;
                    }
                }
`]
})

export class HomeComponent {

}