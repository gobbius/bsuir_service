import {Component, OnInit} from "angular2/core";
import {ROUTER_DIRECTIVES, Router} from "angular2/router";
import {PermissionService} from "../service/permission.service";
import {CookieService} from "angular2-cookie/core";
import {UserService} from "../../login/service/user.service";

@Component({
    selector: "navbar",
    templateUrl: "app/shared/view/navigation.template.html",
    directives: [ROUTER_DIRECTIVES],
    providers: [CookieService],
    styles: [`
            .tab {
                color: black !important;
                background-color: white !important;
            }
            .tab:hover {
                color: white !important;
                background-color: #2196f3 !important;
            }
            .active {
                color: white !important;
                background-color: #2196f3 !important;
            }
            .btn-group {
                margin-top: 1%;
            }
            .user {
                margin-top: -2%;
            }
            .nav-container{
                padding-left: 0px !important;
            }
`]
})

export class NavbarComponent implements OnInit {
    
    constructor(private _permissionService:PermissionService,
                private _cookieService:CookieService,
                private _userService:UserService,
                private _router:Router) {

    }

    get isInternal():boolean {
        return this._permissionService.isInternal;
    }

    get currentUser():string {
        return this._permissionService.currentUser;
    }

    get ordersCount():number{
        return this._permissionService.userOrdersCount;
    }

    set isInternal(value:boolean) {
        this._permissionService.isInternal = value;
    }

    private loadJquery() {
        var interval = setInterval(() => {
            $(".tab").click(($event)=> {
                var currentTab = $event.target;
                $(".tab").each((index, elem)=> {
                    if ($(elem).text() != "Home") {
                        $(elem).removeClass("active");
                    }
                });
                $(currentTab).addClass("active");
            });
            if (document.getElementsByClassName("tab").length > 0) {
                clearInterval(interval);
                if ( !_.isEmpty(this._permissionService.authorization) ){
                    this._userService.startSheduller(this._permissionService.authorization);
                }
            }
        }, 100);
        
    }


    ngOnInit() {
        this.loadJquery();
    }

    logout() {
        this._cookieService.removeAll();
        this._userService.stopSheduler();
        this._permissionService.userOrdersCount = null;
        this._permissionService.isInternal = false;
        this._router.navigate(['Home']);
    }
}