System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var CustomConfig;
    return {
        setters:[],
        execute: function() {
            CustomConfig = (function () {
                function CustomConfig() {
                }
                Object.defineProperty(CustomConfig, "SERVICE_ENDPOINT", {
                    get: function () {
                        return "http://hotelapis.azurewebsites.net";
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(CustomConfig, "API_PREFIX_MAP", {
                    get: function () {
                        return {
                            hotels: "/api/hotel/getallhotels",
                            hotel: "/api/hotel/gethotel?id=",
                            signup: "/api/account/registration",
                            filter: "/api/reservation/getfreehotels",
                            order: "/api/orders/addOrder",
                            orders: "/api/orders/getorders",
                            orderscount: "/api/orders/getorderscount"
                        };
                    },
                    enumerable: true,
                    configurable: true
                });
                return CustomConfig;
            }());
            exports_1("CustomConfig", CustomConfig);
        }
    }
});
//# sourceMappingURL=config.js.map