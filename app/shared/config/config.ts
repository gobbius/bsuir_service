export class CustomConfig {
    public static get SERVICE_ENDPOINT():string {
        return "http://hotelapis.azurewebsites.net";
    }

    public static get API_PREFIX_MAP():{[key:string]:string;} {
        return {
            hotels:"/api/hotel/getallhotels",
            hotel:"/api/hotel/gethotel?id=",
            signup: "/api/account/registration",
            filter: "/api/reservation/getfreehotels",
            order: "/api/orders/addOrder",
            orders: "/api/orders/getorders",
            orderscount: "/api/orders/getorderscount"
        };
    }

}