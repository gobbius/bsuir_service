import {Injectable, OnInit} from "angular2/core";
import {CookieService} from "angular2-cookie/core";
import {UserService} from "../../login/service/user.service";

@Injectable()
export class PermissionService {

    private _isInternal:boolean = false;
    private _authorization:string;
    private _currentUser:string;
    private _userOrdersCount:number;

    constructor(private _cookieService:CookieService) {
        this.loadCookie();
    }

    get isInternal():boolean {
        return this._isInternal;
    }

    set isInternal(value:boolean) {
        this._isInternal = value;
    }

    get authorization():string {
        return this._authorization;
    }

    set authorization(value:string) {
        this._authorization = value;
    }

    get currentUser():string {
        return this._currentUser;
    }

    set currentUser(value:string) {
        this._currentUser = value;
    }

    get userOrdersCount():number {
        return this._userOrdersCount;
    }

    set userOrdersCount(value:number) {
        this._userOrdersCount = value;
    }

    private loadCookie() {
        if (!_.isEmpty(this._cookieService.get("access_token"))) {
            this.isInternal = true;
            this._authorization = this._cookieService.get("token_type") + " " + this._cookieService.get("access_token");
            this._currentUser = _.isUndefined(this._cookieService.get("current_user")) ? "noname"
                : <string>this._cookieService.get("current_user").toLowerCase();
        }
    }
    
}