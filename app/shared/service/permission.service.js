System.register(["angular2/core", "angular2-cookie/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, core_2;
    var PermissionService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (core_2_1) {
                core_2 = core_2_1;
            }],
        execute: function() {
            PermissionService = (function () {
                function PermissionService(_cookieService) {
                    this._cookieService = _cookieService;
                    this._isInternal = false;
                    this.loadCookie();
                }
                Object.defineProperty(PermissionService.prototype, "isInternal", {
                    get: function () {
                        return this._isInternal;
                    },
                    set: function (value) {
                        this._isInternal = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(PermissionService.prototype, "authorization", {
                    get: function () {
                        return this._authorization;
                    },
                    set: function (value) {
                        this._authorization = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(PermissionService.prototype, "currentUser", {
                    get: function () {
                        return this._currentUser;
                    },
                    set: function (value) {
                        this._currentUser = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(PermissionService.prototype, "userOrdersCount", {
                    get: function () {
                        return this._userOrdersCount;
                    },
                    set: function (value) {
                        this._userOrdersCount = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                PermissionService.prototype.loadCookie = function () {
                    if (!_.isEmpty(this._cookieService.get("access_token"))) {
                        this.isInternal = true;
                        this._authorization = this._cookieService.get("token_type") + " " + this._cookieService.get("access_token");
                        this._currentUser = _.isUndefined(this._cookieService.get("current_user")) ? "noname"
                            : this._cookieService.get("current_user").toLowerCase();
                    }
                };
                PermissionService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [core_2.CookieService])
                ], PermissionService);
                return PermissionService;
            }());
            exports_1("PermissionService", PermissionService);
        }
    }
});
//# sourceMappingURL=permission.service.js.map