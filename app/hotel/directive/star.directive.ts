import {Directive, ElementRef, Renderer} from "angular2/core";

@Directive({
    selector: "[stars]",
    host: {
        "(mouseover)": "onHover()",
        "(mouseout)": "onOut()"
    }
})

export class StarDirective {

    constructor(private el:ElementRef, private renderer:Renderer) {

    }

    onHover() {
        var stars = this.el.nativeElement.childNodes[1];
        for (var i = 0; i < stars.childNodes.length; i++) {
            if( stars.childNodes[i].tagName == "LI" ){
                var star = stars.childNodes[i].childNodes[1];
                this.renderer.setElementClass(star, "fa-star-o", false);
                this.renderer.setElementClass(star, "fa-star", true);
            }
        }
    }

    onOut() {
        var stars = this.el.nativeElement.childNodes[1];
        for (var i = 0; i < stars.childNodes.length; i++) {
            if( stars.childNodes[i].tagName == "LI" ){
                var star = stars.childNodes[i].childNodes[1];
                this.renderer.setElementClass(star, "fa-star-o", true);
                this.renderer.setElementClass(star, "fa-star", false);
            }
        }
    }

}