System.register(["angular2/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var DropDownDirective;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            DropDownDirective = (function () {
                function DropDownDirective(el, renderer) {
                    this.el = el;
                    this.renderer = renderer;
                }
                DropDownDirective.prototype.onClick = function () {
                    var arrow = null;
                    _.forEach(this.el.nativeElement.childNodes, function (el) {
                        if (el["tagName"] == "I") {
                            arrow = el;
                        }
                    });
                    if (arrow.className.indexOf("fa-angle-down") > 0) {
                        this.renderer.setElementClass(arrow, "fa-angle-up", true);
                        this.renderer.setElementClass(arrow, "fa-angle-down", false);
                    }
                    else {
                        this.renderer.setElementClass(arrow, "fa-angle-up", false);
                        this.renderer.setElementClass(arrow, "fa-angle-down", true);
                    }
                };
                DropDownDirective = __decorate([
                    core_1.Directive({
                        selector: "[dropdown]",
                        host: {
                            "(click)": "onClick()",
                        }
                    }), 
                    __metadata('design:paramtypes', [core_1.ElementRef, core_1.Renderer])
                ], DropDownDirective);
                return DropDownDirective;
            }());
            exports_1("DropDownDirective", DropDownDirective);
        }
    }
});
//# sourceMappingURL=dropdown.directive.js.map