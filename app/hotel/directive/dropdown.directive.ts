import {Directive, ElementRef, Renderer} from "angular2/core";

@Directive({
    selector: "[dropdown]",
    host: {
        "(click)": "onClick()",
    }
})

export class DropDownDirective {

    constructor(private el:ElementRef, private renderer:Renderer) {

    }

    onClick() {

        var arrow = null;
        _.forEach(this.el.nativeElement.childNodes, el => {
            if ( el["tagName"] == "I" ){
                arrow = el;
            }
        });
        if (arrow.className.indexOf("fa-angle-down") > 0){
            this.renderer.setElementClass(arrow, "fa-angle-up", true);
            this.renderer.setElementClass(arrow, "fa-angle-down", false);
        }else{
            this.renderer.setElementClass(arrow, "fa-angle-up", false);
            this.renderer.setElementClass(arrow, "fa-angle-down", true);
        }

    }

}