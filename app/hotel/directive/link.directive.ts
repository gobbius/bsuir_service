import {Directive, ElementRef, Renderer} from "angular2/core";

@Directive({
    selector: "[link]",
    host: {
        "(mouseover)": "onHover()",
        "(mouseout)": "onOut()"
    }
})

export class LinkDirective {

    constructor(private el:ElementRef, private renderer:Renderer) {

    }

    onHover() {
        this.renderer.setElementStyle(this.el.nativeElement, "color", "white");
    }

    onOut() {
        this.renderer.setElementStyle(this.el.nativeElement, "color", "inherit");
    }

}