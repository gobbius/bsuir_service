System.register(["angular2/core", "../../login/service/user.service", "../../shared/component/spinner.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, user_service_1, spinner_component_1;
    var OrdersComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            },
            function (spinner_component_1_1) {
                spinner_component_1 = spinner_component_1_1;
            }],
        execute: function() {
            OrdersComponent = (function () {
                function OrdersComponent(_userService) {
                    this._userService = _userService;
                    this._isDataLoading = true;
                }
                Object.defineProperty(OrdersComponent.prototype, "orders", {
                    get: function () {
                        return this._orders;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(OrdersComponent.prototype, "isDataLoading", {
                    get: function () {
                        return this._isDataLoading;
                    },
                    enumerable: true,
                    configurable: true
                });
                OrdersComponent.prototype.ngOnInit = function () {
                    this.loadOrders();
                };
                OrdersComponent.prototype.loadOrders = function () {
                    var _this = this;
                    this._userService.getUserOrders()
                        .subscribe(function (result) {
                        _this._orders = result;
                    }, function (error) {
                        console.log(error);
                        _this._isDataLoading = false;
                    }, function () {
                        _this._isDataLoading = false;
                    });
                };
                OrdersComponent = __decorate([
                    core_1.Component({
                        selector: "orders",
                        templateUrl: "app/hotel/view/orders.template.html",
                        providers: [user_service_1.UserService],
                        directives: [spinner_component_1.SpinnerComponent],
                        styles: ["\n                \n                @media (min-width: 992px) {\n                    .custom-container {\n                        margin-top: 5% !important;\n                    }\n                }\n                .custom-container {\n                    margin-top: 35%;\n                }\n                table th{\n                    text-align: center !important;\n                }\n"]
                    }), 
                    __metadata('design:paramtypes', [user_service_1.UserService])
                ], OrdersComponent);
                return OrdersComponent;
            }());
            exports_1("OrdersComponent", OrdersComponent);
        }
    }
});
//# sourceMappingURL=orders.component.js.map