import {Component, OnInit} from "angular2/core";
import {HotelService} from "../service/hotel.service";
import {Hotel} from "../domain/Hotel";
import {PaginationComponent} from "../../shared/component/pagination.component";
import {SpinnerComponent} from "../../shared/component/spinner.component";
import {ROUTER_DIRECTIVES} from "angular2/router";
import {StarDirective} from "../directive/star.directive";
import {LinkDirective} from "../directive/link.directive";
import {DropDownDirective} from "../directive/dropdown.directive";
import {FilterService} from "../service/filter.service";


@Component({
    selector: "all-hotels",
    templateUrl: "app/hotel/view/hotels.template.html",
    providers: [HotelService, FilterService],
    directives: [PaginationComponent, SpinnerComponent, StarDirective,
        ROUTER_DIRECTIVES, LinkDirective, DropDownDirective],
    styles: [`
            @media (min-width: 992px) {
                .custom-container {
                    margin-top: 5% !important;
                }
                .sort-btn {
                    margin-top: 20% !important;
                    width: 100%;
                }
            }
            
            .search-input {
                border-bottom: 3px solid whitesmoke;
                margin-bottom: 1%;
            }
            
            .sort-btn {
                margin-top: 20px;
            }
            .pointer {
                cursor:pointer;
            }
            
            .one-element {
                width: 50%;
                margin: 0 auto;
            }
            
            .custom-container {
                margin-top: 35%;
            }
            .details {
                right: 2%;
                left: 50%;
            }
            .position-fixed{
                position: fixed;
            }
            .spinner-fixed{
                right: 25%; margin-top: 7%;
            }
            .hotels	li	{
                cursor:	default;
            }
            .hotels	li:hover	{
                background:	#ecf0f1;
                cursor:pointer;
            }
            .list-group-item.active,
            .list-group-item.active:hover,
            .list-group-item.active:focus	{
                background-color:	#ecf0f1;
                border-color:	#ecf0f1;
                color:	#2c3e50;
            }
            .stars {
                margin-right: 2%;
                float: right;
            }
            .stars li {
                display: inline;
            }
            .star-active>i {
                color: orange;
            }
            .star-active>i:before{
                content: "\\f005";
            }
            .image {
                width: 100% !important;
                height: 300px !important;
}
`]
})

export class HotelsComponent implements OnInit {

    private _hotels:Hotel[];
    private _hotelsOnPage:Hotel[];
    private _selectedHotel:Hotel;
    private _isDataLoading:boolean = true;
    private _isFilterNeed:boolean = false;
    private _currentOffset:number = 0;
    private _currentEnd:number = 10;
    private _isFilteredByDate:boolean = false;

    constructor(private _hotelService:HotelService, private _filterService:FilterService) {
    }

    get hotelsOnPage():Hotel[] {
        return this._hotelsOnPage;
    }


    get selectedHotel():Hotel {
        return this._selectedHotel;
    }

    get isDataLoading():boolean {
        return this._isDataLoading;
    }

    get hotels():Hotel[] {
        return this._hotels;
    }

    get isFilterNeed():boolean {
        return this._isFilterNeed;
    }


    ngOnInit() {
        this.loadHotels();
        this.initTodayDate();
        this.initDatePickers();
        this.initStarFilter();
    }

    private initTodayDate() {
        var today = new Date();
        var dd = today.getDate().toString();
        var mm = (today.getMonth() + 1).toString(); //January is 0!
        var yyyy = today.getFullYear().toString();

        if (parseInt(dd) < 10) {
            dd = '0' + dd;
        }

        if (parseInt(mm) < 10) {
            mm = '0' + mm;
        }
        var todayString = null;
        todayString = yyyy + '-' + mm + '-' + dd;
        $("#enteringD").val(todayString);
        $("#leavingD").val(todayString);
    }

    private loadHotels() {
        this._hotelService.getHotels()
            .subscribe(result => {
                    this._hotels = result;
                    this._filterService.items = this._hotels;
                    this._hotelsOnPage = !_.isEmpty(this._hotels) ?
                        this._hotels.slice(0, 10) : [];
                },
                error=> {
                    console.error(error);
                    this._isFilteredByDate = false;
                    this._isDataLoading = false;
                },
                () => {
                    this._isDataLoading = false;
                    this._isFilteredByDate = false;
                });
    }

    private initDatePickers() {
        $("#entering, #leaving")["datepicker"]({
            format: "yyyy-mm-dd",
            orientation: "bottom auto",
            todayHighlight: true,
            toggleActive: true
        });
    }

    private initStarFilter() {
        $(".star-radio").change(($event) => {
            var currentFilter = $event.target;
            $(".star-radio").each((index, obj) => {
                $(obj).removeAttr("checked");
            });
            $(currentFilter).prop("checked", true);
        });
    }

    changePage($event) {
        this._currentOffset = $event.offset;
        this._currentEnd = $event.end;
        this._hotelsOnPage = this._hotels.slice($event.offset, $event.end);
        $(".star-radio").each((index, obj) => {
            $(obj).removeAttr("checked");
        });
        $("#search").val("");
    }

    sortByDate() {
        this._isDataLoading = true;
        this._selectedHotel = undefined;
        this._hotels.forEach((el) => {
            if (el.isSelected == true) {
                el.isSelected = false;
            }
        });
        this._filterService.filterByDate($("#enteringD").val(), $("#leavingD").val())
            .subscribe(
                result => {
                    this._hotels = result;
                    this._hotelsOnPage = this._hotels.slice(0, 10);
                    this._isFilteredByDate = true;
                }, error=> {
                    console.error(error);
                    this._isDataLoading = false;
                }
                , () => {
                    this._isDataLoading = false;
                })
    }

    searchHotel($event) {
        if (_.isEmpty($event.target.value)) {
            this._hotelsOnPage = this._hotels.slice(this._currentOffset, this._currentEnd);
            return;
        }
        this._hotelsOnPage = this._filterService.filterByName($event.target.value);
    }

    filterByStars(amountOfStars:number) {
        this._hotelsOnPage = this._filterService.filterByStars(amountOfStars);
    }

    selectHotel(hotel) {

        this._hotels.forEach((el) => {
            if (el.isSelected == true) {
                el.isSelected = false;
            }
        });
        hotel.isSelected = true;
        this._selectedHotel = hotel;

    }

    resetFilters() {
        if (this._isFilteredByDate) {
            this._isDataLoading = true;
            this._selectedHotel = undefined;
            this._hotels.forEach((el) => {
                if (el.isSelected == true) {
                    el.isSelected = false;
                }
            });
            $(".star-radio").each((index, obj) => {
                $(obj).removeAttr("checked");
            });
            $("#search").val("");
            this.loadHotels();
        } else {
            this._hotelsOnPage = this._hotels.slice(this._currentOffset, this._currentEnd);
            $(".star-radio").each((index, obj) => {
                $(obj).removeAttr("checked");
            });
            $("#search").val("");
        }
    }

    addFilter() {
        this._isFilterNeed = !this._isFilterNeed;
    }
}