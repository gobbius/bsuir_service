System.register(["angular2/core", "../service/hotel.service", "../../shared/component/pagination.component", "../../shared/component/spinner.component", "angular2/router", "../directive/star.directive", "../directive/link.directive", "../directive/dropdown.directive", "../service/filter.service"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, hotel_service_1, pagination_component_1, spinner_component_1, router_1, star_directive_1, link_directive_1, dropdown_directive_1, filter_service_1;
    var HotelsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (hotel_service_1_1) {
                hotel_service_1 = hotel_service_1_1;
            },
            function (pagination_component_1_1) {
                pagination_component_1 = pagination_component_1_1;
            },
            function (spinner_component_1_1) {
                spinner_component_1 = spinner_component_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (star_directive_1_1) {
                star_directive_1 = star_directive_1_1;
            },
            function (link_directive_1_1) {
                link_directive_1 = link_directive_1_1;
            },
            function (dropdown_directive_1_1) {
                dropdown_directive_1 = dropdown_directive_1_1;
            },
            function (filter_service_1_1) {
                filter_service_1 = filter_service_1_1;
            }],
        execute: function() {
            HotelsComponent = (function () {
                function HotelsComponent(_hotelService, _filterService) {
                    this._hotelService = _hotelService;
                    this._filterService = _filterService;
                    this._isDataLoading = true;
                    this._isFilterNeed = false;
                    this._currentOffset = 0;
                    this._currentEnd = 10;
                    this._isFilteredByDate = false;
                }
                Object.defineProperty(HotelsComponent.prototype, "hotelsOnPage", {
                    get: function () {
                        return this._hotelsOnPage;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HotelsComponent.prototype, "selectedHotel", {
                    get: function () {
                        return this._selectedHotel;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HotelsComponent.prototype, "isDataLoading", {
                    get: function () {
                        return this._isDataLoading;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HotelsComponent.prototype, "hotels", {
                    get: function () {
                        return this._hotels;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HotelsComponent.prototype, "isFilterNeed", {
                    get: function () {
                        return this._isFilterNeed;
                    },
                    enumerable: true,
                    configurable: true
                });
                HotelsComponent.prototype.ngOnInit = function () {
                    this.loadHotels();
                    this.initTodayDate();
                    this.initDatePickers();
                    this.initStarFilter();
                };
                HotelsComponent.prototype.initTodayDate = function () {
                    var today = new Date();
                    var dd = today.getDate().toString();
                    var mm = (today.getMonth() + 1).toString(); //January is 0!
                    var yyyy = today.getFullYear().toString();
                    if (parseInt(dd) < 10) {
                        dd = '0' + dd;
                    }
                    if (parseInt(mm) < 10) {
                        mm = '0' + mm;
                    }
                    var todayString = null;
                    todayString = yyyy + '-' + mm + '-' + dd;
                    $("#enteringD").val(todayString);
                    $("#leavingD").val(todayString);
                };
                HotelsComponent.prototype.loadHotels = function () {
                    var _this = this;
                    this._hotelService.getHotels()
                        .subscribe(function (result) {
                        _this._hotels = result;
                        _this._filterService.items = _this._hotels;
                        _this._hotelsOnPage = !_.isEmpty(_this._hotels) ?
                            _this._hotels.slice(0, 10) : [];
                    }, function (error) {
                        console.error(error);
                        _this._isFilteredByDate = false;
                        _this._isDataLoading = false;
                    }, function () {
                        _this._isDataLoading = false;
                        _this._isFilteredByDate = false;
                    });
                };
                HotelsComponent.prototype.initDatePickers = function () {
                    $("#entering, #leaving")["datepicker"]({
                        format: "yyyy-mm-dd",
                        orientation: "bottom auto",
                        todayHighlight: true,
                        toggleActive: true
                    });
                };
                HotelsComponent.prototype.initStarFilter = function () {
                    $(".star-radio").change(function ($event) {
                        var currentFilter = $event.target;
                        $(".star-radio").each(function (index, obj) {
                            $(obj).removeAttr("checked");
                        });
                        $(currentFilter).prop("checked", true);
                    });
                };
                HotelsComponent.prototype.changePage = function ($event) {
                    this._currentOffset = $event.offset;
                    this._currentEnd = $event.end;
                    this._hotelsOnPage = this._hotels.slice($event.offset, $event.end);
                    $(".star-radio").each(function (index, obj) {
                        $(obj).removeAttr("checked");
                    });
                    $("#search").val("");
                };
                HotelsComponent.prototype.sortByDate = function () {
                    var _this = this;
                    this._isDataLoading = true;
                    this._selectedHotel = undefined;
                    this._hotels.forEach(function (el) {
                        if (el.isSelected == true) {
                            el.isSelected = false;
                        }
                    });
                    this._filterService.filterByDate($("#enteringD").val(), $("#leavingD").val())
                        .subscribe(function (result) {
                        _this._hotels = result;
                        _this._hotelsOnPage = _this._hotels.slice(0, 10);
                        _this._isFilteredByDate = true;
                    }, function (error) {
                        console.error(error);
                        _this._isDataLoading = false;
                    }, function () {
                        _this._isDataLoading = false;
                    });
                };
                HotelsComponent.prototype.searchHotel = function ($event) {
                    if (_.isEmpty($event.target.value)) {
                        this._hotelsOnPage = this._hotels.slice(this._currentOffset, this._currentEnd);
                        return;
                    }
                    this._hotelsOnPage = this._filterService.filterByName($event.target.value);
                };
                HotelsComponent.prototype.filterByStars = function (amountOfStars) {
                    this._hotelsOnPage = this._filterService.filterByStars(amountOfStars);
                };
                HotelsComponent.prototype.selectHotel = function (hotel) {
                    this._hotels.forEach(function (el) {
                        if (el.isSelected == true) {
                            el.isSelected = false;
                        }
                    });
                    hotel.isSelected = true;
                    this._selectedHotel = hotel;
                };
                HotelsComponent.prototype.resetFilters = function () {
                    if (this._isFilteredByDate) {
                        this._isDataLoading = true;
                        this._selectedHotel = undefined;
                        this._hotels.forEach(function (el) {
                            if (el.isSelected == true) {
                                el.isSelected = false;
                            }
                        });
                        $(".star-radio").each(function (index, obj) {
                            $(obj).removeAttr("checked");
                        });
                        $("#search").val("");
                        this.loadHotels();
                    }
                    else {
                        this._hotelsOnPage = this._hotels.slice(this._currentOffset, this._currentEnd);
                        $(".star-radio").each(function (index, obj) {
                            $(obj).removeAttr("checked");
                        });
                        $("#search").val("");
                    }
                };
                HotelsComponent.prototype.addFilter = function () {
                    this._isFilterNeed = !this._isFilterNeed;
                };
                HotelsComponent = __decorate([
                    core_1.Component({
                        selector: "all-hotels",
                        templateUrl: "app/hotel/view/hotels.template.html",
                        providers: [hotel_service_1.HotelService, filter_service_1.FilterService],
                        directives: [pagination_component_1.PaginationComponent, spinner_component_1.SpinnerComponent, star_directive_1.StarDirective,
                            router_1.ROUTER_DIRECTIVES, link_directive_1.LinkDirective, dropdown_directive_1.DropDownDirective],
                        styles: ["\n            @media (min-width: 992px) {\n                .custom-container {\n                    margin-top: 5% !important;\n                }\n                .sort-btn {\n                    margin-top: 20% !important;\n                    width: 100%;\n                }\n            }\n            \n            .search-input {\n                border-bottom: 3px solid whitesmoke;\n                margin-bottom: 1%;\n            }\n            \n            .sort-btn {\n                margin-top: 20px;\n            }\n            .pointer {\n                cursor:pointer;\n            }\n            \n            .one-element {\n                width: 50%;\n                margin: 0 auto;\n            }\n            \n            .custom-container {\n                margin-top: 35%;\n            }\n            .details {\n                right: 2%;\n                left: 50%;\n            }\n            .position-fixed{\n                position: fixed;\n            }\n            .spinner-fixed{\n                right: 25%; margin-top: 7%;\n            }\n            .hotels\tli\t{\n                cursor:\tdefault;\n            }\n            .hotels\tli:hover\t{\n                background:\t#ecf0f1;\n                cursor:pointer;\n            }\n            .list-group-item.active,\n            .list-group-item.active:hover,\n            .list-group-item.active:focus\t{\n                background-color:\t#ecf0f1;\n                border-color:\t#ecf0f1;\n                color:\t#2c3e50;\n            }\n            .stars {\n                margin-right: 2%;\n                float: right;\n            }\n            .stars li {\n                display: inline;\n            }\n            .star-active>i {\n                color: orange;\n            }\n            .star-active>i:before{\n                content: \"\\f005\";\n            }\n            .image {\n                width: 100% !important;\n                height: 300px !important;\n}\n"]
                    }), 
                    __metadata('design:paramtypes', [hotel_service_1.HotelService, filter_service_1.FilterService])
                ], HotelsComponent);
                return HotelsComponent;
            }());
            exports_1("HotelsComponent", HotelsComponent);
        }
    }
});
//# sourceMappingURL=hotels.component.js.map