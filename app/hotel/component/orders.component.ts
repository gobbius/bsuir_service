import {Component, OnInit} from "angular2/core";
import {UserService} from "../../login/service/user.service";
import {Order} from "../domain/Order";
import {SpinnerComponent} from "../../shared/component/spinner.component";

@Component({
    selector: "orders",
    templateUrl: "app/hotel/view/orders.template.html",
    providers: [UserService],
    directives: [SpinnerComponent],
    styles: [`
                
                @media (min-width: 992px) {
                    .custom-container {
                        margin-top: 5% !important;
                    }
                }
                .custom-container {
                    margin-top: 35%;
                }
                table th{
                    text-align: center !important;
                }
`]
})

export class OrdersComponent implements OnInit{

    private _orders:Order[];
    private _isDataLoading:boolean = true;

    constructor(private _userService:UserService) {
    }

    get orders():Order[] {
        return this._orders;
    }

    get isDataLoading():boolean {
        return this._isDataLoading;
    }

    ngOnInit() {
        this.loadOrders();
    }
    
    private loadOrders() {
        this._userService.getUserOrders()
            .subscribe(result => {
                this._orders = result;
            }, error => {
                console.log(error);
                this._isDataLoading = false;
            }, () => {
                this._isDataLoading = false;
            });
    }
}