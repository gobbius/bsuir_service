System.register(["angular2/core", "angular2/router", "../service/hotel.service", "../../shared/component/spinner.component", "../directive/dropdown.directive", "../domain/Order", "../../shared/service/permission.service"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, hotel_service_1, spinner_component_1, dropdown_directive_1, Order_1, permission_service_1;
    var HotelComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (hotel_service_1_1) {
                hotel_service_1 = hotel_service_1_1;
            },
            function (spinner_component_1_1) {
                spinner_component_1 = spinner_component_1_1;
            },
            function (dropdown_directive_1_1) {
                dropdown_directive_1 = dropdown_directive_1_1;
            },
            function (Order_1_1) {
                Order_1 = Order_1_1;
            },
            function (permission_service_1_1) {
                permission_service_1 = permission_service_1_1;
            }],
        execute: function() {
            HotelComponent = (function () {
                function HotelComponent(_hotelService, _routeParams, _router, _permissionService) {
                    this._hotelService = _hotelService;
                    this._routeParams = _routeParams;
                    this._router = _router;
                    this._permissionService = _permissionService;
                    this._isDataLoading = true;
                    this._isRoomsShown = false;
                    this._isConditionsShown = false;
                    this._totalPrice = 0;
                    this._isBooked = false;
                    this._isError = false;
                    this._isDatesInvalid = false;
                    this._images = [];
                }
                Object.defineProperty(HotelComponent.prototype, "hotel", {
                    get: function () {
                        return this._hotel;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HotelComponent.prototype, "isDataLoading", {
                    get: function () {
                        return this._isDataLoading;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HotelComponent.prototype, "images", {
                    get: function () {
                        return this._images;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HotelComponent.prototype, "isConditionsShown", {
                    get: function () {
                        return this._isConditionsShown;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HotelComponent.prototype, "isRoomsShown", {
                    get: function () {
                        return this._isRoomsShown;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HotelComponent.prototype, "totalPrice", {
                    get: function () {
                        return this._totalPrice;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HotelComponent.prototype, "isBooked", {
                    get: function () {
                        return this._isBooked;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HotelComponent.prototype, "isError", {
                    get: function () {
                        return this._isError;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HotelComponent.prototype, "isDatesInvalid", {
                    get: function () {
                        return this._isDatesInvalid;
                    },
                    enumerable: true,
                    configurable: true
                });
                HotelComponent.prototype.ngOnInit = function () {
                    this.loadHotel(this._routeParams.get("id"));
                    this.initDatePickers();
                    $("#bookBtn").click(function () {
                        $(this).toggleClass("active");
                    });
                };
                HotelComponent.prototype.loadHotel = function (hotelId) {
                    var _this = this;
                    this._hotelService.getHotel(hotelId)
                        .subscribe(function (result) {
                        _this._hotel = result;
                        if (!_.isUndefined(_this._hotel.images)) {
                            _this._hotel.images.forEach(function (el, index) {
                                var image = { isActive: false, url: el };
                                if (index == 0) {
                                    image.isActive = true;
                                }
                                _this._images.push(image);
                            });
                        }
                    }, function (error) {
                        console.log(error);
                        _this._router.navigate(['../../../NotFound']);
                    }, function () {
                        _this._isDataLoading = false;
                    });
                };
                HotelComponent.prototype.initImageModal = function () {
                    $(".openImage").click(function ($event) {
                        var imageUrl = $($event.target).attr("src");
                        $("#imagepreview").attr("src", imageUrl);
                        $('#imagemodal')["modal"]().show();
                    });
                };
                HotelComponent.prototype.showRooms = function () {
                    var _this = this;
                    this._isRoomsShown = !this._isRoomsShown;
                    _.delay(function () {
                        _this.initImageModal();
                    }, 100);
                };
                HotelComponent.prototype.initDatePickers = function () {
                    $("#arrival, #leaving")["datepicker"]({
                        format: "yyyy-mm-dd",
                        orientation: "bottom auto",
                        todayHighlight: true,
                        toggleActive: true
                    });
                };
                HotelComponent.prototype.showBookModal = function () {
                    $("#bookmodal")["modal"]().show();
                };
                HotelComponent.prototype.bookHotel = function (form) {
                    var _this = this;
                    $("#bookBtn").prop("disabled", true);
                    var order = new Order_1.Order({});
                    order.hotelName = this._hotel.name;
                    order.userName = this._permissionService.currentUser;
                    order.roomNumber = form.roomNumber;
                    order.countOfDays = form.days;
                    order.arrivalDate = $("#arrival").val();
                    order.leaveDate = $("#leaving").val();
                    order.totalPrice = this._totalPrice;
                    if (_.isEmpty(order.arrivalDate) || _.isEmpty(order.leaveDate)) {
                        this._isDatesInvalid = true;
                        $("#bookBtn").toggleClass("active");
                        $("#bookBtn").prop("disabled", false);
                        _.delay(function () {
                            _this._isDatesInvalid = false;
                        }, 4000);
                        return;
                    }
                    this._hotelService.makeOrder(order)
                        .subscribe(function (result) {
                        console.log(result);
                        _this._isBooked = true;
                        _.delay(function () {
                            _this._isBooked = false;
                        }, 4000);
                    }, function (error) {
                        console.error(error);
                        var errorJson = JSON.parse(error["_body"]);
                        if (errorJson.message.indexOf("Not added") >= 0) {
                            _this._isError = true;
                            _.delay(function () {
                                _this._isError = false;
                            }, 4000);
                        }
                        $("#bookBtn").toggleClass("active");
                        $("#bookBtn").prop("disabled", false);
                    }, function () {
                        $("#bookBtn").toggleClass("active");
                        $("#bookBtn").prop("disabled", false);
                    });
                };
                HotelComponent.prototype.countTotalPirce = function (days, roomnumber) {
                    var roomPrice = 0;
                    _.forEach(this._hotel.rooms, function (obj) {
                        if (obj.number == roomnumber) {
                            roomPrice = obj.price;
                        }
                    });
                    this._totalPrice = roomPrice * days;
                };
                HotelComponent.prototype.showConditions = function () {
                    var _this = this;
                    this._isConditionsShown = !this._isConditionsShown;
                    _.delay(function () {
                        _this.initImageModal();
                    }, 100);
                };
                HotelComponent = __decorate([
                    core_1.Component({
                        selector: "hotel",
                        templateUrl: "app/hotel/view/hotel.template.html",
                        directives: [spinner_component_1.SpinnerComponent, dropdown_directive_1.DropDownDirective],
                        providers: [hotel_service_1.HotelService],
                        styles: ["\n                .spinner {\n                    display: inline-block;\n                    opacity: 0;\n                    width: 0;\n                \n                    -webkit-transition: opacity 0.25s, width 0.25s;\n                    -moz-transition: opacity 0.25s, width 0.25s;\n                    -o-transition: opacity 0.25s, width 0.25s;\n                    transition: opacity 0.25s, width 0.25s;\n                }\n                .has-spinner.active {\n                    cursor:progress;\n                }\n                \n                .has-spinner.active .spinner {\n                    opacity: 1;\n                    width: auto; /* This doesn't work, just fix for unkown width elements */\n                }\n                \n                .has-spinner.btn-mini.active .spinner {\n                    width: 10px;\n                }\n                \n                .has-spinner.btn-small.active .spinner {\n                    width: 13px;\n                }\n                \n                .has-spinner.btn.active .spinner {\n                    width: 16px;\n                }\n                \n                .has-spinner.btn-large.active .spinner {\n                        width: 19px;\n                }\n                .modal-book-btn {\n                    width: 30%;\n                }\n                .modal-footer{\n                    text-align: left !important;\n                }\n                .m-header{\n                    display: inline;\n                }\n                .book-btn {\n                    margin-top: 5%;\n                }\n                .right {\n                    cursor: pointer;\n                }\n                .left {\n                    cursor:pointer;\n                }\n                .carousel {\n                    width: 85%;\n                }\n                .custom-slider {\n                    padding-bottom: 5%;\n                }\n                \n                @media (min-width: 992px) {\n                    .custom-container {\n                        margin-top: 5% !important;\n                    }\n                    .book-btn {\n                        margin-top: 1%;\n                    }\n                }\n                \n                .custom-container {\n                    margin-top: 25%;\n                }\n                \n                .custom-image {\n                    height: 400px !important;\n                    max-height: 400px!important;\n                    width: 100%;\n                    margin: 0 auto;\n                }\n                .stars li {\n                    display: inline;\n                }\n                .stars {\n                    margin-bottom: 0px;\n                    padding-left: 0px !important;\n                }\n                .star-active>i {\n                    color: orange;\n                }\n                .star-active>i:before{\n                    content: \"\\f005\";\n                }\n                .float-right {\n                    float: right;\n                }\n                .modal-image {\n                    width: 100%;\n                }\n"]
                    }), 
                    __metadata('design:paramtypes', [hotel_service_1.HotelService, router_1.RouteParams, router_1.Router, permission_service_1.PermissionService])
                ], HotelComponent);
                return HotelComponent;
            }());
            exports_1("HotelComponent", HotelComponent);
        }
    }
});
//# sourceMappingURL=hotel.component.js.map