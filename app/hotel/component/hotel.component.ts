import {Component, OnInit} from "angular2/core";
import {RouteParams, Router} from "angular2/router";
import {HotelService} from "../service/hotel.service";
import {Hotel} from "../domain/Hotel";
import {SpinnerComponent} from "../../shared/component/spinner.component";
import {DropDownDirective} from "../directive/dropdown.directive";
import {Order} from "../domain/Order";
import {PermissionService} from "../../shared/service/permission.service";

@Component({
    selector: "hotel",
    templateUrl: "app/hotel/view/hotel.template.html",
    directives: [SpinnerComponent, DropDownDirective],
    providers: [HotelService],
    styles: [`
                .spinner {
                    display: inline-block;
                    opacity: 0;
                    width: 0;
                
                    -webkit-transition: opacity 0.25s, width 0.25s;
                    -moz-transition: opacity 0.25s, width 0.25s;
                    -o-transition: opacity 0.25s, width 0.25s;
                    transition: opacity 0.25s, width 0.25s;
                }
                .has-spinner.active {
                    cursor:progress;
                }
                
                .has-spinner.active .spinner {
                    opacity: 1;
                    width: auto; /* This doesn't work, just fix for unkown width elements */
                }
                
                .has-spinner.btn-mini.active .spinner {
                    width: 10px;
                }
                
                .has-spinner.btn-small.active .spinner {
                    width: 13px;
                }
                
                .has-spinner.btn.active .spinner {
                    width: 16px;
                }
                
                .has-spinner.btn-large.active .spinner {
                        width: 19px;
                }
                .modal-book-btn {
                    width: 30%;
                }
                .modal-footer{
                    text-align: left !important;
                }
                .m-header{
                    display: inline;
                }
                .book-btn {
                    margin-top: 5%;
                }
                .right {
                    cursor: pointer;
                }
                .left {
                    cursor:pointer;
                }
                .carousel {
                    width: 85%;
                }
                .custom-slider {
                    padding-bottom: 5%;
                }
                
                @media (min-width: 992px) {
                    .custom-container {
                        margin-top: 5% !important;
                    }
                    .book-btn {
                        margin-top: 1%;
                    }
                }
                
                .custom-container {
                    margin-top: 25%;
                }
                
                .custom-image {
                    height: 400px !important;
                    max-height: 400px!important;
                    width: 100%;
                    margin: 0 auto;
                }
                .stars li {
                    display: inline;
                }
                .stars {
                    margin-bottom: 0px;
                    padding-left: 0px !important;
                }
                .star-active>i {
                    color: orange;
                }
                .star-active>i:before{
                    content: "\\f005";
                }
                .float-right {
                    float: right;
                }
                .modal-image {
                    width: 100%;
                }
`]
})

export class HotelComponent implements OnInit {

    private _hotel:Hotel;
    private _isDataLoading:boolean = true;
    private _images:any[];
    private _isRoomsShown:boolean = false;
    private _isConditionsShown:boolean = false;
    private _totalPrice:number = 0;
    private _isBooked:boolean = false;
    private _isError:boolean = false;
    private _isDatesInvalid:boolean = false;

    constructor(private _hotelService:HotelService,
                private _routeParams:RouteParams,
                private _router:Router,
                private _permissionService:PermissionService) {
        this._images = [];
    }

    get hotel():Hotel {
        return this._hotel;
    }

    get isDataLoading():boolean {
        return this._isDataLoading;
    }

    get images():any[] {
        return this._images;
    }

    get isConditionsShown():boolean {
        return this._isConditionsShown;
    }

    get isRoomsShown():boolean {
        return this._isRoomsShown;
    }

    get totalPrice():number {
        return this._totalPrice;
    }

    get isBooked():boolean {
        return this._isBooked;
    }

    get isError():boolean {
        return this._isError;
    }

    get isDatesInvalid():boolean {
        return this._isDatesInvalid;
    }

    ngOnInit() {
        this.loadHotel(this._routeParams.get("id"));
        this.initDatePickers();
        $("#bookBtn").click(function () {
            $(this).toggleClass("active");
        });
    }

    private loadHotel(hotelId) {
        this._hotelService.getHotel(hotelId)
            .subscribe(
                result => {
                    this._hotel = result;
                    if (!_.isUndefined(this._hotel.images)) {
                        this._hotel.images.forEach((el, index)=> {
                            var image = {isActive: false, url: el};
                            if (index == 0) {
                                image.isActive = true;
                            }
                            this._images.push(image);
                        });
                    }
                },
                error=> {
                    console.log(error);
                    this._router.navigate(['../../../NotFound']);
                },
                () => {
                    this._isDataLoading = false;
                });
    }

    private initImageModal() {
        $(".openImage").click(($event) => {
            var imageUrl = $($event.target).attr("src");
            $("#imagepreview").attr("src", imageUrl);
            $('#imagemodal')["modal"]().show();
        });
    }

    showRooms() {
        this._isRoomsShown = !this._isRoomsShown;
        _.delay(()=> {
            this.initImageModal();
        }, 100);
    }

    private initDatePickers() {
        $("#arrival, #leaving")["datepicker"]({
            format: "yyyy-mm-dd",
            orientation: "bottom auto",
            todayHighlight: true,
            toggleActive: true
        });
    }

    showBookModal() {
        $("#bookmodal")["modal"]().show();
    }

    bookHotel(form) {
        $("#bookBtn").prop("disabled", true);

        var order:Order = new Order({});
        order.hotelName = this._hotel.name;
        order.userName = this._permissionService.currentUser;
        order.roomNumber = form.roomNumber;
        order.countOfDays = form.days;
        order.arrivalDate = $("#arrival").val();
        order.leaveDate = $("#leaving").val();
        order.totalPrice = this._totalPrice;

        if ( _.isEmpty(order.arrivalDate) || _.isEmpty(order.leaveDate) ){
            this._isDatesInvalid = true;
            $("#bookBtn").toggleClass("active");
            $("#bookBtn").prop("disabled", false);
            _.delay(() => {
                this._isDatesInvalid = false;
            }, 4000);
            return;
        }

        this._hotelService.makeOrder(order)
            .subscribe(result => {
                    console.log(result);
                    this._isBooked = true;
                    _.delay(() => {
                        this._isBooked = false;
                    }, 4000);
                },
                error => {
                    console.error(error);
                    var errorJson = JSON.parse(error["_body"]);
                    if (errorJson.message.indexOf("Not added") >= 0) {
                        this._isError = true;
                        _.delay(() => {
                            this._isError = false;
                        }, 4000);
                    }
                    $("#bookBtn").toggleClass("active");
                    $("#bookBtn").prop("disabled", false);
                },
                () => {
                    $("#bookBtn").toggleClass("active");
                    $("#bookBtn").prop("disabled", false);
                });


    }

    countTotalPirce(days, roomnumber) {
        var roomPrice = 0;
        _.forEach(this._hotel.rooms, (obj) => {
            if (obj.number == roomnumber) {
                roomPrice = obj.price;
            }
        });

        this._totalPrice = roomPrice * days;
    }

    showConditions() {
        this._isConditionsShown = !this._isConditionsShown;
        _.delay(()=> {
            this.initImageModal();
        }, 100);
    }

}