export class Condition {

    private _id:number;
    private _images:any[];
    private _name:string;

    constructor(params:{[key:string]:any;}) {
    }
    
    get name():string {
        return this._name;
    }

    set name(value:string) {
        this._name = value;
    }
    get images():any[] {
        return this._images;
    }

    set images(value:any[]) {
        this._images = value;
    }
    get id():number {
        return this._id;
    }

    set id(value:number) {
        this._id = value;
    }
}