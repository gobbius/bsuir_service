export class Room {

    private _id:number;
    private _description:string;
    private _hotelName:string;
    private _images:any[];
    private _number:number;
    private _personCount:number;
    private _price:number;

    constructor(params:{[key:string]:any;}) {
    }

    get price():number {
        return this._price;
    }

    set price(value:number) {
        this._price = value;
    }
    get personCount():number {
        return this._personCount;
    }

    set personCount(value:number) {
        this._personCount = value;
    }
    get number():number {
        return this._number;
    }

    set number(value:number) {
        this._number = value;
    }
    get images():any[] {
        return this._images;
    }

    set images(value:any[]) {
        this._images = value;
    }
    get hotelName():string {
        return this._hotelName;
    }

    set hotelName(value:string) {
        this._hotelName = value;
    }
    get description():string {
        return this._description;
    }

    set description(value:string) {
        this._description = value;
    }
    get id():number {
        return this._id;
    }

    set id(value:number) {
        this._id = value;
    }


}