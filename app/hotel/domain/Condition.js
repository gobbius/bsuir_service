System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Condition;
    return {
        setters:[],
        execute: function() {
            Condition = (function () {
                function Condition(params) {
                }
                Object.defineProperty(Condition.prototype, "name", {
                    get: function () {
                        return this._name;
                    },
                    set: function (value) {
                        this._name = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Condition.prototype, "images", {
                    get: function () {
                        return this._images;
                    },
                    set: function (value) {
                        this._images = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Condition.prototype, "id", {
                    get: function () {
                        return this._id;
                    },
                    set: function (value) {
                        this._id = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                return Condition;
            }());
            exports_1("Condition", Condition);
        }
    }
});
//# sourceMappingURL=Condition.js.map