export class Order {

    id:number;
    hotelName:string;
    userName:string;
    roomNumber:number;
    countOfDays:number;
    arrivalDate:Date;
    leaveDate:Date;
    totalPrice:number

    constructor(params:{[key:string]:any;}) {
    }
    
}