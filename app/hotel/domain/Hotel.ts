import {Condition} from "./Condition";
import {Room} from "./Room";
export class Hotel {

    private _id:number;
    private _name:string;
    private _address:string;
    private _description:string;
    private _conditions:Condition[] = [];
    private _images:any[] = [];
    private _rooms:Room[] = [];
    private _stars:any[] = [];
    private _isSelected:boolean;

    constructor(params:{[key:string]:any;}) {
    }

    get isSelected():boolean {
        return this._isSelected;
    }

    set isSelected(value:boolean) {
        this._isSelected = value;
    }
    get stars():any[] {
        return this._stars;
    }

    set stars(value:any[]) {
        this._stars = value;
    }
    get rooms():Room[] {
        return this._rooms;
    }

    set rooms(value:Room[]) {
        this._rooms = value;
    }
    get images():any[] {
        return this._images;
    }

    set images(value:any[]) {
        this._images = value;
    }
    get conditions():Condition[] {
        return this._conditions;
    }

    set conditions(value:Condition[]) {
        this._conditions = value;
    }
    get description():string {
        return this._description;
    }

    set description(value:string) {
        this._description = value;
    }
    get address():string {
        return this._address;
    }

    set address(value:string) {
        this._address = value;
    }
    get name():string {
        return this._name;
    }

    set name(value:string) {
        this._name = value;
    }
    get id():number {
        return this._id;
    }

    set id(value:number) {
        this._id = value;
    }

}