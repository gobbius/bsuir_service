System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Hotel;
    return {
        setters:[],
        execute: function() {
            Hotel = (function () {
                function Hotel(params) {
                    this._conditions = [];
                    this._images = [];
                    this._rooms = [];
                    this._stars = [];
                }
                Object.defineProperty(Hotel.prototype, "isSelected", {
                    get: function () {
                        return this._isSelected;
                    },
                    set: function (value) {
                        this._isSelected = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Hotel.prototype, "stars", {
                    get: function () {
                        return this._stars;
                    },
                    set: function (value) {
                        this._stars = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Hotel.prototype, "rooms", {
                    get: function () {
                        return this._rooms;
                    },
                    set: function (value) {
                        this._rooms = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Hotel.prototype, "images", {
                    get: function () {
                        return this._images;
                    },
                    set: function (value) {
                        this._images = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Hotel.prototype, "conditions", {
                    get: function () {
                        return this._conditions;
                    },
                    set: function (value) {
                        this._conditions = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Hotel.prototype, "description", {
                    get: function () {
                        return this._description;
                    },
                    set: function (value) {
                        this._description = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Hotel.prototype, "address", {
                    get: function () {
                        return this._address;
                    },
                    set: function (value) {
                        this._address = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Hotel.prototype, "name", {
                    get: function () {
                        return this._name;
                    },
                    set: function (value) {
                        this._name = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Hotel.prototype, "id", {
                    get: function () {
                        return this._id;
                    },
                    set: function (value) {
                        this._id = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                return Hotel;
            }());
            exports_1("Hotel", Hotel);
        }
    }
});
//# sourceMappingURL=Hotel.js.map