System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Room;
    return {
        setters:[],
        execute: function() {
            Room = (function () {
                function Room(params) {
                }
                Object.defineProperty(Room.prototype, "price", {
                    get: function () {
                        return this._price;
                    },
                    set: function (value) {
                        this._price = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Room.prototype, "personCount", {
                    get: function () {
                        return this._personCount;
                    },
                    set: function (value) {
                        this._personCount = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Room.prototype, "number", {
                    get: function () {
                        return this._number;
                    },
                    set: function (value) {
                        this._number = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Room.prototype, "images", {
                    get: function () {
                        return this._images;
                    },
                    set: function (value) {
                        this._images = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Room.prototype, "hotelName", {
                    get: function () {
                        return this._hotelName;
                    },
                    set: function (value) {
                        this._hotelName = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Room.prototype, "description", {
                    get: function () {
                        return this._description;
                    },
                    set: function (value) {
                        this._description = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Room.prototype, "id", {
                    get: function () {
                        return this._id;
                    },
                    set: function (value) {
                        this._id = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                return Room;
            }());
            exports_1("Room", Room);
        }
    }
});
//# sourceMappingURL=Room.js.map