import {Component} from "angular2/core";
import {RouteConfig, ROUTER_DIRECTIVES, Router, OnActivate, OnDeactivate} from "angular2/router";
import {HotelComponent} from "../component/hotel.component";
import {HotelsComponent} from "../component/hotels.component";
import {PermissionService} from "../../shared/service/permission.service";

@RouteConfig([
    {
        path: "/:id",
        name: "Hotel",
        component: HotelComponent
    },
    {
        path: "/",
        name: "Hotels",
        component: HotelsComponent
    }

])

@Component({
    selector: "hotels",
    template: `<div class='container-fluid'>
                    <router-outlet></router-outlet>
                </div>`,
    directives: [ROUTER_DIRECTIVES]
})

export class HotelRouter implements OnActivate {

    constructor(private _router:Router,
                private _permissionService:PermissionService) {
    }

    routerOnActivate(next, prev) {
        var location = document.location.pathname.split("/");
        var param = null;

        if (location.length == 4) {
            next.urlPath = "hotel";
            param = location[3];
        }
        if (location.length == 2 && location.indexOf("not-found") > 0) {
            next.urlPath = "hotels";
        }
        
        switch (next.urlPath) {
            case "hotels":
            {
                if (this._permissionService.isInternal) {
                    this._router.navigate(["Hotels"]);
                } else {
                    this._router.navigate(["../../Denied"]);
                }
                break;
            }
            case "hotel":
            {
                if (this._permissionService.isInternal) {
                    this._router.navigate(["Hotel", {id: param}]);
                } else {
                    this._router.navigate(["../../Denied"]);
                }
                break;
            }
            default:
            {
                this._router.navigate(["../../NotFound"]);
                break;
            }
        }
    }

}