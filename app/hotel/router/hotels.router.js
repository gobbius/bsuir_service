System.register(["angular2/core", "angular2/router", "../component/hotel.component", "../component/hotels.component", "../../shared/service/permission.service"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, hotel_component_1, hotels_component_1, permission_service_1;
    var HotelRouter;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (hotel_component_1_1) {
                hotel_component_1 = hotel_component_1_1;
            },
            function (hotels_component_1_1) {
                hotels_component_1 = hotels_component_1_1;
            },
            function (permission_service_1_1) {
                permission_service_1 = permission_service_1_1;
            }],
        execute: function() {
            HotelRouter = (function () {
                function HotelRouter(_router, _permissionService) {
                    this._router = _router;
                    this._permissionService = _permissionService;
                }
                HotelRouter.prototype.routerOnActivate = function (next, prev) {
                    var location = document.location.pathname.split("/");
                    var param = null;
                    if (location.length == 4) {
                        next.urlPath = "hotel";
                        param = location[3];
                    }
                    if (location.length == 2 && location.indexOf("not-found") > 0) {
                        next.urlPath = "hotels";
                    }
                    switch (next.urlPath) {
                        case "hotels":
                            {
                                if (this._permissionService.isInternal) {
                                    this._router.navigate(["Hotels"]);
                                }
                                else {
                                    this._router.navigate(["../../Denied"]);
                                }
                                break;
                            }
                        case "hotel":
                            {
                                if (this._permissionService.isInternal) {
                                    this._router.navigate(["Hotel", { id: param }]);
                                }
                                else {
                                    this._router.navigate(["../../Denied"]);
                                }
                                break;
                            }
                        default:
                            {
                                this._router.navigate(["../../NotFound"]);
                                break;
                            }
                    }
                };
                HotelRouter = __decorate([
                    router_1.RouteConfig([
                        {
                            path: "/:id",
                            name: "Hotel",
                            component: hotel_component_1.HotelComponent
                        },
                        {
                            path: "/",
                            name: "Hotels",
                            component: hotels_component_1.HotelsComponent
                        }
                    ]),
                    core_1.Component({
                        selector: "hotels",
                        template: "<div class='container-fluid'>\n                    <router-outlet></router-outlet>\n                </div>",
                        directives: [router_1.ROUTER_DIRECTIVES]
                    }), 
                    __metadata('design:paramtypes', [router_1.Router, permission_service_1.PermissionService])
                ], HotelRouter);
                return HotelRouter;
            }());
            exports_1("HotelRouter", HotelRouter);
        }
    }
});
//# sourceMappingURL=hotels.router.js.map