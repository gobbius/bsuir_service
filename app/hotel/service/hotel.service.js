System.register(["angular2/http", "angular2/core", "../../shared/config/config", "../domain/Hotel", "../../shared/service/permission.service", "../domain/Condition", "../domain/Room"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var http_1, core_1, config_1, Hotel_1, permission_service_1, Condition_1, Room_1;
    var HotelService;
    return {
        setters:[
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (config_1_1) {
                config_1 = config_1_1;
            },
            function (Hotel_1_1) {
                Hotel_1 = Hotel_1_1;
            },
            function (permission_service_1_1) {
                permission_service_1 = permission_service_1_1;
            },
            function (Condition_1_1) {
                Condition_1 = Condition_1_1;
            },
            function (Room_1_1) {
                Room_1 = Room_1_1;
            }],
        execute: function() {
            HotelService = (function () {
                function HotelService(_http, _permissionService) {
                    this._http = _http;
                    this._permissionService = _permissionService;
                    this._serviceUrl = config_1.CustomConfig.SERVICE_ENDPOINT;
                    this._headers = new http_1.Headers({
                        "Authorization": this._permissionService.authorization,
                        "Accept": "application/json"
                    });
                }
                HotelService.prototype.ngOnInit = function () {
                };
                HotelService.prototype.getHotels = function () {
                    var hotelsUrl = this._serviceUrl + config_1.CustomConfig.API_PREFIX_MAP["hotels"];
                    return this._http.get(hotelsUrl, {
                        headers: this._headers
                    }).map(function (result) {
                        var hotels = [];
                        var resultJson = result.json();
                        resultJson.forEach(function (el) {
                            var tempHotel = new Hotel_1.Hotel({});
                            tempHotel.id = el["Id"];
                            tempHotel.name = el["Name"];
                            tempHotel.address = el["Adress"];
                            tempHotel.description = el["Description"];
                            tempHotel.images = el["Images"];
                            tempHotel.stars = new Array(el["Stars"]);
                            if (!_.isNull(el["Conditions"])) {
                                el["Conditions"].forEach(function (cond) {
                                    var tempCond = new Condition_1.Condition({});
                                    tempCond.id = cond["Id"];
                                    tempCond.images = cond["Images"];
                                    tempCond.name = cond["Name"];
                                    tempHotel.conditions.push(tempCond);
                                });
                            }
                            if (!_.isNull(el["Rooms"])) {
                                el["Rooms"].forEach(function (room) {
                                    var tempRoom = new Room_1.Room({});
                                    tempRoom.id = room["Id"];
                                    tempRoom.images = room["Images"];
                                    tempRoom.hotelName = room["HotelName"];
                                    tempRoom.number = room["Number"];
                                    tempRoom.personCount = room["PersonCount"];
                                    tempRoom.price = room["Price"];
                                    tempRoom.description = room["Description"];
                                    tempHotel.rooms.push(tempRoom);
                                });
                            }
                            hotels.push(tempHotel);
                        });
                        return hotels;
                    });
                };
                HotelService.prototype.getHotel = function (hotelId) {
                    var hotelsUrl = this._serviceUrl + config_1.CustomConfig.API_PREFIX_MAP["hotel"] + hotelId;
                    return this._http.get(hotelsUrl, {
                        headers: this._headers
                    }).map(function (res) {
                        var tempHotel = new Hotel_1.Hotel({});
                        var result = res.json();
                        tempHotel.id = result["id"];
                        tempHotel.name = result["name"];
                        tempHotel.address = result["adress"];
                        tempHotel.description = result["description"];
                        tempHotel.images = result["images"];
                        tempHotel.stars = new Array(result["stars"]);
                        if (!_.isNull(result["conditions"])) {
                            result["conditions"].forEach(function (cond) {
                                var tempCond = new Condition_1.Condition({});
                                tempCond.id = cond["id"];
                                tempCond.images = cond["images"];
                                tempCond.name = cond["name"];
                                tempHotel.conditions.push(tempCond);
                            });
                        }
                        if (!_.isNull(result["rooms"])) {
                            result["rooms"].forEach(function (room) {
                                var tempRoom = new Room_1.Room({});
                                tempRoom.id = room["id"];
                                tempRoom.images = room["images"];
                                tempRoom.hotelName = room["hotelName"];
                                tempRoom.number = room["number"];
                                tempRoom.personCount = room["personcount"];
                                tempRoom.price = room["price"];
                                tempRoom.description = room["description"];
                                tempHotel.rooms.push(tempRoom);
                            });
                        }
                        return tempHotel;
                    });
                };
                HotelService.prototype.makeOrder = function (order) {
                    var orderUrl = this._serviceUrl + config_1.CustomConfig.API_PREFIX_MAP["order"];
                    if (!this._headers.has("Content-Type")) {
                        this._headers.append('Content-Type', 'application/json');
                    }
                    return this._http.post(orderUrl, JSON.stringify(order), {
                        headers: this._headers
                    }).map(function (result) { return result.json(); });
                };
                HotelService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http, permission_service_1.PermissionService])
                ], HotelService);
                return HotelService;
            }());
            exports_1("HotelService", HotelService);
        }
    }
});
//# sourceMappingURL=hotel.service.js.map