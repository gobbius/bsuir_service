System.register(["angular2/core", "angular2/http", "../../shared/service/permission.service", "../../shared/config/config", "../domain/Hotel", "../domain/Condition", "../domain/Room"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1, permission_service_1, config_1, Hotel_1, Condition_1, Room_1;
    var FilterService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (permission_service_1_1) {
                permission_service_1 = permission_service_1_1;
            },
            function (config_1_1) {
                config_1 = config_1_1;
            },
            function (Hotel_1_1) {
                Hotel_1 = Hotel_1_1;
            },
            function (Condition_1_1) {
                Condition_1 = Condition_1_1;
            },
            function (Room_1_1) {
                Room_1 = Room_1_1;
            }],
        execute: function() {
            FilterService = (function () {
                function FilterService(_http, _permissionService) {
                    this._http = _http;
                    this._permissionService = _permissionService;
                    this._serviceUrl = config_1.CustomConfig.SERVICE_ENDPOINT;
                    this._headers = new http_1.Headers({
                        "Authorization": this._permissionService.authorization,
                        "Accept": "application/json"
                    });
                }
                Object.defineProperty(FilterService.prototype, "items", {
                    set: function (value) {
                        this._items = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                FilterService.prototype.filterByDate = function (from, to) {
                    var filterUrl = this._serviceUrl + config_1.CustomConfig.API_PREFIX_MAP["filter"];
                    // var currentHours = new Date().getHours() < 10 ? "0" + new Date().getHours() : new Date().getHours();
                    // var currentMinutes = new Date().getMinutes() < 10 ? "0" + new Date().getMinutes() : new Date().getMinutes();
                    // from += "T" + currentHours + ":" + currentMinutes + "Z";
                    // to += "T" + currentHours + ":" + currentMinutes + "Z";
                    var body = "?from=" + from + "&to=" + to;
                    filterUrl = filterUrl + body;
                    return this._http.get(filterUrl, {
                        headers: this._headers
                    }).map(function (result) {
                        var hotels = [];
                        var resultJson = result.json();
                        resultJson.forEach(function (obj) {
                            var tempHotel = new Hotel_1.Hotel({});
                            tempHotel.id = obj["id"];
                            tempHotel.name = obj["name"];
                            tempHotel.address = obj["adress"];
                            tempHotel.description = obj["description"];
                            tempHotel.images = obj["images"];
                            tempHotel.stars = new Array(obj["stars"]);
                            if (!_.isNull(obj["conditions"])) {
                                obj["conditions"].forEach(function (cond) {
                                    var tempCond = new Condition_1.Condition({});
                                    tempCond.id = cond["id"];
                                    tempCond.images = cond["images"];
                                    tempCond.name = cond["name"];
                                    tempHotel.conditions.push(tempCond);
                                });
                            }
                            if (!_.isNull(obj["rooms"])) {
                                obj["rooms"].forEach(function (room) {
                                    var tempRoom = new Room_1.Room({});
                                    tempRoom.id = room["id"];
                                    tempRoom.images = room["images"];
                                    tempRoom.hotelName = room["hotelName"];
                                    tempRoom.number = room["number"];
                                    tempRoom.personCount = room["personCount"];
                                    tempRoom.price = room["price"];
                                    tempRoom.description = room["description"];
                                    tempHotel.rooms.push(tempRoom);
                                });
                            }
                            hotels.push(tempHotel);
                        });
                        return hotels;
                    });
                };
                FilterService.prototype.filterByName = function (name) {
                    var hotelsToReturn = [];
                    for (var _i = 0, _a = this._items; _i < _a.length; _i++) {
                        var hotel = _a[_i];
                        if (hotel.name.toLowerCase().indexOf(name.toLowerCase()) >= 0) {
                            hotelsToReturn.push(hotel);
                        }
                    }
                    return hotelsToReturn;
                };
                FilterService.prototype.filterByStars = function (amountOfStars) {
                    var hotelsToReturn = [];
                    for (var _i = 0, _a = this._items; _i < _a.length; _i++) {
                        var hotel = _a[_i];
                        if (hotel.stars.length == amountOfStars) {
                            hotelsToReturn.push(hotel);
                        }
                    }
                    return hotelsToReturn;
                };
                FilterService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http, permission_service_1.PermissionService])
                ], FilterService);
                return FilterService;
            }());
            exports_1("FilterService", FilterService);
        }
    }
});
//# sourceMappingURL=filter.service.js.map