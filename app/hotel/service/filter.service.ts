import {Injectable} from "angular2/core";
import {Http, Headers} from "angular2/http";
import {PermissionService} from "../../shared/service/permission.service";
import {CustomConfig} from "../../shared/config/config";
import {Observable} from "rxjs/Observable";
import {Hotel} from "../domain/Hotel";
import {Condition} from "../domain/Condition";
import {Room} from "../domain/Room";

@Injectable()
export class FilterService {

    private _serviceUrl:string = CustomConfig.SERVICE_ENDPOINT;
    private _items:Hotel[];

    private _headers:Headers = new Headers({
        "Authorization": this._permissionService.authorization,
        "Accept": "application/json"
    });

    constructor(private _http:Http, private _permissionService:PermissionService) {

    }

    set items(value:any) {
        this._items = value;
    }

    filterByDate(from:string, to:string):Observable<any> {

        var filterUrl = this._serviceUrl + CustomConfig.API_PREFIX_MAP["filter"];
        // var currentHours = new Date().getHours() < 10 ? "0" + new Date().getHours() : new Date().getHours();
        // var currentMinutes = new Date().getMinutes() < 10 ? "0" + new Date().getMinutes() : new Date().getMinutes();
        // from += "T" + currentHours + ":" + currentMinutes + "Z";
        // to += "T" + currentHours + ":" + currentMinutes + "Z";

        var body = "?from=" + from + "&to=" + to;
        filterUrl = filterUrl + body;
        return this._http.get(filterUrl, {
            headers: this._headers
        }).map(result => {
            var hotels:Hotel[] = [];
            var resultJson = result.json();
            resultJson.forEach(obj => {
                var tempHotel = new Hotel({});
                tempHotel.id = obj["id"];
                tempHotel.name = obj["name"];
                tempHotel.address = obj["adress"];
                tempHotel.description = obj["description"];
                tempHotel.images = obj["images"];
                tempHotel.stars = new Array<any>(<number>obj["stars"]);
                if (!_.isNull(obj["conditions"])) {
                    obj["conditions"].forEach(cond => {
                        var tempCond = new Condition({});
                        tempCond.id = cond["id"];
                        tempCond.images = cond["images"];
                        tempCond.name = cond["name"]
                        tempHotel.conditions.push(tempCond);
                    });
                }
                if (!_.isNull(obj["rooms"])) {
                    obj["rooms"].forEach(room => {
                        var tempRoom = new Room({});
                        tempRoom.id = room["id"];
                        tempRoom.images = room["images"];
                        tempRoom.hotelName = room["hotelName"];
                        tempRoom.number = room["number"];
                        tempRoom.personCount = room["personCount"];
                        tempRoom.price = room["price"];
                        tempRoom.description = room["description"];
                        tempHotel.rooms.push(tempRoom);
                    });
                }

                hotels.push(tempHotel);
            });
            return hotels;
        });
    }


    filterByName(name:string):Hotel[] {
        var hotelsToReturn = [];
        for (var hotel of this._items) {
            if (hotel.name.toLowerCase().indexOf(name.toLowerCase()) >= 0) {
                hotelsToReturn.push(hotel);
            }
        }
        return hotelsToReturn;
    }

    filterByStars(amountOfStars:number):Hotel[] {
        var hotelsToReturn = [];
        for (var hotel of this._items) {
            if (hotel.stars.length == amountOfStars) {
                hotelsToReturn.push(hotel);
            }
        }
        return hotelsToReturn;

    }
}