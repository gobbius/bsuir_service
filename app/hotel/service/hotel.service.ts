import {Http, Headers} from "angular2/http";
import {Injectable, OnInit} from "angular2/core";
import {Observable} from "rxjs/Rx";

import {CustomConfig} from "../../shared/config/config";
import {Hotel} from "../domain/Hotel";
import {PermissionService} from "../../shared/service/permission.service";
import {Condition} from "../domain/Condition";
import {Room} from "../domain/Room";
import {Order} from "../domain/Order";

@Injectable()
export class HotelService implements OnInit {

    private _serviceUrl:string = CustomConfig.SERVICE_ENDPOINT;

    private _headers:Headers = new Headers({
        "Authorization": this._permissionService.authorization,
        "Accept": "application/json"
    });

    constructor(private _http:Http, private _permissionService:PermissionService) {
    }

    ngOnInit() {
    }

    getHotels():Observable<Hotel[]> {
        var hotelsUrl = this._serviceUrl + CustomConfig.API_PREFIX_MAP["hotels"];
        return this._http.get(hotelsUrl, {
            headers: this._headers
        }).map(result => {
            var hotels:Hotel[] = [];
            var resultJson = result.json();

            resultJson.forEach(el => {
                var tempHotel = new Hotel({});
                tempHotel.id = el["Id"];
                tempHotel.name = el["Name"];
                tempHotel.address = el["Adress"];
                tempHotel.description = el["Description"];
                tempHotel.images = el["Images"];
                tempHotel.stars = new Array<any>(<number>el["Stars"]);
                if (!_.isNull(el["Conditions"])) {
                    el["Conditions"].forEach(cond => {
                        var tempCond = new Condition({});
                        tempCond.id = cond["Id"];
                        tempCond.images = cond["Images"];
                        tempCond.name = cond["Name"]
                        tempHotel.conditions.push(tempCond);
                    });
                }
                if (!_.isNull(el["Rooms"])) {
                    el["Rooms"].forEach(room => {
                        var tempRoom = new Room({});
                        tempRoom.id = room["Id"];
                        tempRoom.images = room["Images"];
                        tempRoom.hotelName = room["HotelName"];
                        tempRoom.number = room["Number"];
                        tempRoom.personCount = room["PersonCount"];
                        tempRoom.price = room["Price"];
                        tempRoom.description = room["Description"];
                        tempHotel.rooms.push(tempRoom);
                    });
                }

                hotels.push(tempHotel);
            });

            return hotels;
        });
    }

    getHotel(hotelId):Observable<Hotel> {
        var hotelsUrl = this._serviceUrl + CustomConfig.API_PREFIX_MAP["hotel"] + hotelId;
        return this._http.get(hotelsUrl, {
            headers: this._headers
        }).map(res => {
            var tempHotel = new Hotel({});
            var result = res.json();
            tempHotel.id = result["id"];
            tempHotel.name = result["name"];
            tempHotel.address = result["adress"];
            tempHotel.description = result["description"];
            tempHotel.images = result["images"];
            tempHotel.stars = new Array<any>(<number>result["stars"]);
            if (!_.isNull(result["conditions"])) {
                result["conditions"].forEach(cond => {
                    var tempCond = new Condition({});
                    tempCond.id = cond["id"];
                    tempCond.images = cond["images"];
                    tempCond.name = cond["name"]
                    tempHotel.conditions.push(tempCond);
                });
            }

            if (!_.isNull(result["rooms"])) {
                result["rooms"].forEach(room => {
                    var tempRoom = new Room({});
                    tempRoom.id = room["id"];
                    tempRoom.images = room["images"];
                    tempRoom.hotelName = room["hotelName"];
                    tempRoom.number = room["number"];
                    tempRoom.personCount = room["personcount"];
                    tempRoom.price = room["price"];
                    tempRoom.description = room["description"];
                    tempHotel.rooms.push(tempRoom);
                });
            }
            return tempHotel;
        });
    }

    makeOrder(order:Order):Observable<any> {

        var orderUrl = this._serviceUrl + CustomConfig.API_PREFIX_MAP["order"];
        if (!this._headers.has("Content-Type")) {
            this._headers.append('Content-Type', 'application/json');
        }

        return this._http.post(orderUrl, JSON.stringify(order), {
            headers: this._headers
        }).map(result => result.json());
    }
}