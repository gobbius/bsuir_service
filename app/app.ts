import {Component} from "angular2/core";
import {RouteConfig} from "angular2/router";
import {NotFoundComponent} from "./shared/component/notfound.component";
import {HomeComponent} from "./shared/component/home.component";
import {NavigationComponent} from "./shared/router/navigation.router";
import {PermissionsDeniedComponent} from "./shared/component/permission-denied.component";
import {NavbarComponent} from "./shared/component/navbar.component";


@RouteConfig([
    {
        path: "/not-found",
        name: "NotFound",
        component: NotFoundComponent
    },
    {
        path: "/navigation/...",
        name: "Navigation",
        component: NavigationComponent
    },
    {
        path: "/",
        name: "Home",
        component: HomeComponent,
        useAsDefault: true
    },
    {
        path: "/*other",
        name: "Other",
        redirectTo: ["Home"]
    },
    {
        path: "/denied",
        name: "Denied",
        component: PermissionsDeniedComponent,
    }
])

@Component({
    selector: "main",
    templateUrl: "app/app.template.html",
    directives: [NavbarComponent]
})
export class App {

    constructor () {
    }


}