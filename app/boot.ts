import {bootstrap}    from 'angular2/platform/browser'
import {App} from './app'
import {HTTP_PROVIDERS} from "angular2/http";
import {ROUTER_PROVIDERS} from "angular2/router";
import {PermissionService} from "./shared/service/permission.service";
import {CookieService} from "angular2-cookie/core";
import {UserService} from "./login/service/user.service";

bootstrap(App, [HTTP_PROVIDERS, ROUTER_PROVIDERS, PermissionService, CookieService, UserService])
    .catch(error => {
        console.error(error);
    });