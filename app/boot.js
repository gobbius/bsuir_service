System.register(['angular2/platform/browser', './app', "angular2/http", "angular2/router", "./shared/service/permission.service", "angular2-cookie/core", "./login/service/user.service"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var browser_1, app_1, http_1, router_1, permission_service_1, core_1, user_service_1;
    return {
        setters:[
            function (browser_1_1) {
                browser_1 = browser_1_1;
            },
            function (app_1_1) {
                app_1 = app_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (permission_service_1_1) {
                permission_service_1 = permission_service_1_1;
            },
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            }],
        execute: function() {
            browser_1.bootstrap(app_1.App, [http_1.HTTP_PROVIDERS, router_1.ROUTER_PROVIDERS, permission_service_1.PermissionService, core_1.CookieService, user_service_1.UserService])
                .catch(function (error) {
                console.error(error);
            });
        }
    }
});
//# sourceMappingURL=boot.js.map